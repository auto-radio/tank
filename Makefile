##
##  tank, Import and Playlist Daemon for Aura project
##  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Affero General Public License as
##  published by the Free Software Foundation, either version 3 of the
##  License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Affero General Public License for more details.
##
##  You should have received a copy of the GNU Affero General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.
##

GOCMD := go
ifdef GOROOT
	GOCMD = $(GOROOT)/bin/go
endif

ifndef $(GOPATH)
    GOPATH=$(shell go env GOPATH)
    export GOPATH
endif

SWAG = $(GOPATH)/bin/swag
SWAG_ARGS := -d api/v1/,cmd/tank/ -g api.go

EXECUTEABLE := tank

all: build
.PHONY: vet format ui build clean distclean

init.dev::
	go get -u github.com/swaggo/swag/cmd/swag
	go install github.com/swaggo/swag/cmd/swag@latest

vet:
	$(GOCMD) vet ./...

format:
	$(GOCMD) fmt ./...

ui:
	$(GOCMD) generate ./ui

fmt-api-docs:
	$(SWAG) fmt $(SWAG_ARGS)

api-docs:
	$(SWAG) init $(SWAG_ARGS) --pd -o api/docs

# build target actually depends on api-docs
# to allow building binary without generating api docs first, we put api/docs/docs.go under version control
# see #30
build: ui
	$(GOCMD) build -o $(EXECUTEABLE) ./cmd/tank

dev:
	$(GOCMD) build -o $(EXECUTEABLE) -tags=dev ./cmd/tank

clean:
	rm -f $(EXECUTEABLE)

distclean: clean
	rm -f ui/assets_vfsdata.go
