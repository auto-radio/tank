### Build Image
FROM golang:1.20-bookworm AS builder
LABEL maintainer="Christian Pointner <equinox@helsinki.at>"

RUN set -e \
  && apt-get update -q \
  && apt-get install -y -q --no-install-recommends make \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY Makefile go.mod go.sum /usr/src/
COPY api /usr/src/api
COPY auth /usr/src/auth
COPY cmd /usr/src/cmd
COPY importer /usr/src/importer
COPY store /usr/src/store
COPY ui /usr/src/ui

WORKDIR /usr/src
RUN make

### APP Image
FROM debian:bookworm
LABEL maintainer="Christian Pointner <equinox@helsinki.at>"

ENV AURA_UID=2872
ENV AURA_GID=${AURA_UID}

RUN set -e \
  && apt-get update -q \
  && apt-get install -y -q --no-install-recommends ffmpeg ca-certificates netcat-openbsd \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN set -e \
  && mkdir -p /etc/aura /srv/audio /srv/import-temp /srv/logs /var/audio/import \
  && adduser --home /srv --no-create-home --system --uid ${AURA_UID} --group app \
  && chown ${AURA_UID}:${AURA_GID} /srv/audio /srv/import-temp /srv/logs /var/audio/import

COPY contrib/for-docker.yaml /etc/aura/tank.yaml
COPY --from=builder /usr/src/tank /usr/local/bin/tank

VOLUME /srv/audio
VOLUME /srv/import-temp
VOLUME /srv/logs

ENV TANK_STORE_PATH=/srv/audio TANK_IMPORTER_TEMP_PATH=/srv/import-temp TANK_LOGS=/srv/logs
ENV TANK_DB_HOST=172.17.0.1 TANK_DB_USERNAME=tank TANK_DB_PASSWORD=aura TANK_DB_NAME=tank
EXPOSE 8040/tcp

USER app
CMD ["/usr/local/bin/tank", "run", "--listen", ":8040"]
