/*
 *  tank, Import and Playlist Daemon for Aura project
 *  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';

/********************************* utils *********************************/

Number.prototype.pad = function(size) {
  var s = String(this);
  while (s.length < (size || 2)) {s = "0" + s;}
  return s;
};

function format_datetime(d) {
  if(Object.prototype.toString.call(d) !== '[object Date]') {
    return '-';
  }
  if (d.toString() === 'Invalid Date') {
    return '-';
  }
  var datetimestr = Number(d.getDate()).pad(2);
  datetimestr += '.' + Number(d.getMonth() + 1).pad(2);
  datetimestr += '.' + d.getFullYear();
  datetimestr += ' ' + Number(d.getHours()).pad(2);
  datetimestr += ':' + Number(d.getMinutes()).pad(2);
  datetimestr += ':' + Number(d.getSeconds()).pad(2);
  return datetimestr;
}

function format_duration(time) {
  if(isNaN(time)) return '-';

    // duration is in ns -> first convert to ms
  time = Number(Math.floor(time / 1000000));
  var h = Number(Math.floor(time / 3600000));
  time %= 3600000;
  var m = Number(Math.floor(time / 60000));
  time %= 60000;
  var s = Number(Math.floor(time / 1000));
  var hs = Number(Math.floor((time % 1000)/100));

  return h + ':' + m.pad(2) + ':' + s.pad(2) + '.' + hs;
}

/********************************* main *********************************/

var main = main || {};

/***************** models *****************/

/****** authInfo ******/

var auth = null;

main.AuthInfo = function() {
  this.$this = $(this);
  this.enabled = false;
  this.token = "";
  this.session = {};
  this.backends = [];

  if(typeof(Storage) !== "undefined") {
    if(sessionStorage.auth_token) {
      var token = sessionStorage.auth_token;
      if($.type(token) === "string") {
        this.token = token;
      }
    }
  }

}

main.AuthInfo.prototype.init = function() {
  var self = this;

  $.ajax({
    method: "GET",
    url: "/auth/backends",
    datatype: "json",
    success: function(data) {
      self.enabled = true;
      self.backends = data;
      self.$this.trigger('update');
      if(this.token != "") {
        self.fetch();
      }
    }
  }).fail(function(jqxhr, textStatus, error) {
    if(error == "") {
      alert("fetching auth backends failed: connection problem");
    } else {
      if(jqxhr.status != 404) {
        alert("fetching auth backends failed:" + error);
      }
      self.$this.trigger('update');
      self.enabled = false;
      shows.fetch();
    }
  });
}

main.AuthInfo.prototype.new_session = function() {
  var self = this;

  $.ajax({
    method: "POST",
    url: "/auth/session",
    data: JSON.stringify({'backend': 'oidc'}),
    dataType: "json",
    success: function(data) {
      self.token = data.token
      sessionStorage.auth_token = self.token;

      self.session = data.session
      self.$this.trigger('update');
      if(self.session.state == "logged-in") {
        shows.fetch();
      } else {
        self.login()
      }
    }
  }).fail(function(jqxhr, textStatus, error) {
    if(error == "") {
      alert("creating new auth session failed: connection problem");
    } else {
      alert("creating new auth session failed: " + error);
      self.token = "";
      sessionStorage.auth_token = self.token;

      self.session = {};
      self.$this.trigger('update');
      shows.reset();
    }
  });
}

main.AuthInfo.prototype.fetch = function(wait, done) {
  var self = this;

  var url = "/auth/session";
  if(wait) {
    url = url + "?wait-for-login=1";
  }
  $.ajax({
    method: "GET",
    url: url,
    headers: { Authorization: "Bearer " + this.token },
    datatype: "json",
    success: function(data) {
      self.session = data;
      self.$this.trigger('update');
      if(self.session.state == "logged-in") {
        shows.fetch();
      }
    }
  }).fail(function(jqxhr, textStatus, error) {
    if(error == "") {
      alert("fetching auth session failed: connection problem");
    } else {
      self.token = ""
      sessionStorage.auth_token = self.token;
      self.session = {};
      shows.reset();
    }
  }).always(function() {
    if(done) {
      done();
    }
  });
};

main.AuthInfo.prototype.login = function() {
  var self = this;

  var modal = $('#loginModal');

  self.fetch(1, function() {
    modal.modal('hide');
  });

  modal.on('shown.bs.modal',function() {
    $(this).find('iframe').attr('src','/auth/oidc/login?session-id=' + self.session.id)
  })
  modal.on('hide.bs.modal',function() {
    self.fetch();
  })

  modal.modal('show');
};

main.AuthInfo.prototype.logout = function() {
  var self = this;

  $.ajax({
    method: "DELETE",
    url: "/auth/session",
    headers: { Authorization: "Bearer " + this.token },
    dataType: "json",
    success: function(data) {
      self.token = ""
      sessionStorage.auth_token = self.token;
      self.session = {};
      self.$this.trigger('update');
      shows.reset();
    }
  });
};

/****** showList ******/

var shows = null;

main.ShowList = function() {
  this.$this = $(this);
  this.shows = {};
};

main.ShowList.prototype.error = function(error) {
  this.last_error = error;
  this.$this.trigger('error');
};

main.ShowList.prototype.reset = function() {
  this.shows = {};
  this.$this.trigger('update');
}

main.ShowList.prototype.fetch = function() {
  var self = this;
  self.last_error = null;

  $.ajax({
    method: "GET",
    url: "/api/v1/shows",
    headers: { Authorization: "Bearer " + auth.token },
    dataType: "json",
    success: function(data) {
      self.shows = [];
      for(var key in data.results) {
        self.shows[data.results[key].name] = new main.Show(data.results[key]);
      }
      self.$this.trigger('update');
    }
  }).fail(function(jqxhr, textStatus, error) {
    if(error == "") {
      self.error("connection problem");
    } else {
      self.error(error);
    }
  });
};

main.ShowList.prototype.new_show = function(show_name) {
  var self = this;
  self.last_error = null;

  $.ajax({
    method: "POST",
    url: "/api/v1/shows/" + show_name,
    headers: { Authorization: "Bearer " + auth.token },
    dataType: "json",
    success: function(data) {
      self.fetch();
    }
  }).fail(function(jqxhr, textStatus, error) {
    if(error == "") {
      self.error("connection problem");
    } else {
      self.error(jqxhr.responseJSON.error);
    }
  });
}

/****** show ******/

var show = null;

main.Show = function(inst) {
  this.$this = $(this);
  this.name = inst.name;
  this.created = new Date(inst.created);
  this.updated = new Date(inst.updated);
  this.files = [];
  this.playlists = [];
};

main.Show.prototype.error = function(error) {
  this.last_error = error;
  this.$this.trigger('error');
};

main.Show.prototype.clone = function(cloneName) {
  var self = this;
  self.last_error = null;

  $.ajax({
    method: "POST",
    url: "/api/v1/shows/" + cloneName + "?cloneFrom=" + self.name,
    headers: { Authorization: "Bearer " + auth.token },
    dataType: "json",
    success: function(data) {
      shows.fetch();
    }
  }).fail(function(jqxhr, textStatus, error) {
    if(error == "") {
      self.error("connection problem");
    } else {
      self.error(error);
    }
  });
};

main.Show.prototype.delete = function(cloneName) {
  var self = this;
  self.last_error = null;

  $.ajax({
    method: "DELETE",
    url: "/api/v1/shows/" + self.name,
    headers: { Authorization: "Bearer " + auth.token },
    dataType: "json",
    success: function(data) {
      shows.fetch();
    }
  }).fail(function(jqxhr, textStatus, error) {
    if(error == "") {
      self.error("connection problem");
    } else {
      self.error(error);
    }
  });
};

main.Show.prototype.fetch_files = function() {
  var self = this;
  self.last_error = null;

  $.ajax({
    method: "GET",
    url: "/api/v1/shows/" + self.name + "/files",
    headers: { Authorization: "Bearer " + auth.token },
    dataType: "json",
    success: function(data) {
      self.files = [];
      for(var key in data.results) {
        self.files.push(new main.File(self, data.results[key]));
      }
      self.$this.trigger('update-files');
    }
  }).fail(function(jqxhr, textStatus, error) {
    if(error == "") {
      self.error("connection problem");
    } else {
      self.error(error);
    }
  });
};

main.Show.prototype.fetch_playlists = function() {
  var self = this;
  self.last_error = null;

  $.ajax({
    method: "GET",
    url: "/api/v1/shows/" + self.name + "/playlists",
    headers: { Authorization: "Bearer " + auth.token },
    dataType: "json",
    success: function(data) {
      self.playlists = [];
      for(var key in data.results) {
        self.playlists.push(new main.Playlist(self, data.results[key]));
      }
      self.$this.trigger('update-playlists');
    }
  }).fail(function(jqxhr, textStatus, error) {
    if(error == "") {
      self.error("connection problem");
    } else {
      self.error(error);
    }
  });
};

main.Show.prototype.add_file_upload = function(file) {
  var self = this;
  self.last_error = null;

  $.ajax({
    method: "POST",
    url: "/api/v1/shows/" + self.name + "/files",
    headers: { Authorization: "Bearer " + auth.token },
    data: JSON.stringify({'sourceURI': encodeURI('upload://' + file.name)}),
    dataType: "json",
    success: function(data) {
      var f = new main.File(self, data);
      f.start_upload(file);
      self.fetch_files();
    }
  }).fail(function(jqxhr, textStatus, error) {
    if(error == "") {
      self.error("connection problem");
    } else {
      self.error(jqxhr.responseJSON.error);
    }
  });
}

main.Show.prototype.add_file_uri = function(file_uri) {
  var self = this;
  self.last_error = null;

  $.ajax({
    method: "POST",
    url: "/api/v1/shows/" + self.name + "/files",
    headers: { Authorization: "Bearer " + auth.token },
    data: JSON.stringify({'sourceURI': file_uri}),
    dataType: "json",
    success: function(data) {
      self.fetch_files();
    }
  }).fail(function(jqxhr, textStatus, error) {
    if(error == "") {
      self.error("connection problem");
    } else {
      self.error(jqxhr.responseJSON.error);
    }
  });
}

/****** file ******/

main.File = function(show, inst) {
  this.$show = show;
  this.id = inst.id;
  this.created = new Date(inst.created);
  this.updated = new Date(inst.updated);
  this.source = inst.source;
  this.metadata = inst.metadata;
  this.size = inst.size;
  this.duration = inst.duration;
};

main.File.prototype.delete = function() {
  var self = this;

  $.ajax({
    method: "DELETE",
    url: "/api/v1/shows/" + this.$show.name + "/files/" + this.id,
    headers: { Authorization: "Bearer " + auth.token },
    dataType: "json",
    success: function(data) {
      self.$show.fetch_files();
    }
  }).fail(function(jqxhr, textStatus, error) {
    alert(jqxhr.responseJSON.error + "\n" + JSON.stringify(jqxhr.responseJSON.details, null, 2));
  });
};

main.File.prototype.edit = function(metadata) {
  var self = this;

  $.ajax({
    method: "PATCH",
    url: "/api/v1/shows/" + this.$show.name + "/files/" + this.id,
    headers: { Authorization: "Bearer " + auth.token },
    data: JSON.stringify(metadata),
    dataType: "json",
    success: function(data) {
      self.$show.fetch_files();
    }
  }).fail(function(jqxhr, textStatus, error) {
    alert(jqxhr.responseJSON.error);
  });
}

main.File.prototype.start_upload = function(file) {
  var self = this;

  $.ajax({
    method: "GET",
    url: "/api/v1/shows/" + this.$show.name + "/files/" + this.id + "/import",
    headers: { Authorization: "Bearer " + auth.token },
    data: {'waitFor': 'running'},
    dataType: "json",
    success: function(data) {
      self.upload(file);
    }
  }).fail(function(jqxhr, textStatus, error) {
    alert(jqxhr.responseJSON.error);
  });
}

main.File.prototype.upload = function(file) {
  var self = this;

  var flow = new Flow({
    target:"/api/v1/shows/" + this.$show.name + "/files/" + this.id + "/upload",
    chunkSize: 100 * 1024,
    prioritizeFirstAndLastChunk: true,
    headers: { Authorization: "Bearer " + auth.token },
  });
  if(!flow.support) {
    return self.upload_simple(file);
  }
  flow.on('fileSuccess', function(file, message) {
    console.log(file, message);
  });
  flow.on('fileError', function(file, message) {
    console.log(file, message);
  });
  flow.addFile(file);
  flow.upload();
}

main.File.prototype.upload_simple = function(file) {
  var self = this;

  $.ajax({
    method: "PUT",
    url: "/api/v1/shows/" + this.$show.name + "/files/" + this.id + "/upload",
    headers: { Authorization: "Bearer " + auth.token },
    contentType: "application/octet-stream",
    data: file,
    processData: false,
    dataType: "json",
    success: function(data) {
      console.log(data);
    }
  }).fail(function(jqxhr, textStatus, error) {
    alert(jqxhr.responseJSON.error);
  });
}

main.File.prototype.fetch_logs = function(metadata) {
  var self = this;

  $.ajax({
    method: "GET",
    url: "/api/v1/shows/" + this.$show.name + "/files/" + this.id + "/logs",
    headers: { Authorization: "Bearer " + auth.token },
    data: JSON.stringify(metadata),
    dataType: "json",
    success: function(data) {
      console.log(data);
    }
  }).fail(function(jqxhr, textStatus, error) {
    alert(jqxhr.responseJSON.error);
  });
}

/****** playlist ******/

main.Playlist = function(show, inst) {
  this.$show = show;
  this.id = inst.id;
  this.description = inst.description;
  this.created = new Date(inst.created);
  this.updated = new Date(inst.updated);
  this.entries = inst.entries;
};

main.Playlist.prototype.delete = function() {
  var self = this;

  $.ajax({
    method: "DELETE",
    url: "/api/v1/shows/" + this.$show.name + "/playlists/" + this.id,
    headers: { Authorization: "Bearer " + auth.token },
    dataType: "json",
    success: function(data) {
      self.$show.fetch_playlists();
    }
  }).fail(function(jqxhr, textStatus, error) {
    alert(jqxhr.responseJSON.error + "\n" + JSON.stringify(jqxhr.responseJSON.details, null, 2));
  });
};

/***************** views *****************/

/****** authInfo ******/

var authInfoView = null;

main.AuthInfoView = function(model) {
  this.model = model;

  var self = this;
  $(this.model).on('update', function() {
    self.render_info();
  });
};

main.AuthInfoView.prototype.render_info = function() {
  var status = $('#authStatus');
  var btn = $('#btnLogInOut');
  var self = this;

  if (!this.model.enabled) {
    status.html("you are logged in as <b>anonymous</b>");
    btn.hide();
    return;
  }
  btn.show();

  if (this.model.session.state == "logged-in")  {
    status.html("you are logged in as <b>" + this.model.session.username + "</b>");
    btn.text("Logout")
    btn.removeClass('btn-primary').addClass('btn-danger');
    btn.off('click').on('click', function() {
      self.model.logout();
    });
  } else {
    status.html("you are <b>not</b> logged in");
    btn.text("Login")
    btn.removeClass('btn-danger').addClass('btn-primary');
    btn.off('click').on('click', function() {
      self.model.new_session();
    });
  }
};

/****** showList ******/

var showListView = null;

main.ShowListView = function(model) {
  this.model = model;

  var self = this;
  $(this.model).on('update', function() {
    self.render_list();
  });
  $(this.model).on('error', function() {
    self.render_error();
  });
};

main.ShowListView.prototype.render_list = function() {
  $('#error').hide();

  var list = $('#showSelect');
  $('option', list).remove();
  list.append($('<option>', {value:'__none__', text:'choose...'}));
  for (var name in this.model.shows) {
    list.append($('<option>', {value:name, text:name}));
  }
  this.render_buttons('__none__');

  var self = this;
  list.off('change').on('change', function(event) {
    self.render_buttons(event.target.value);
  });
};

main.ShowListView.prototype.render_buttons = function(selected_show) {
  if(Object.keys(this.model.shows).length == 0 || selected_show == '__none__') {
    $('#showView').empty();
    if(!auth.enabled || auth.session.state == "logged-in") {
      $('button.showNew').removeClass('disabled').attr('data-toggle', 'modal');
    } else {
      $('button.showNew').addClass('disabled').removeAttr('data-toggle');
    }
    $('button.showClone').addClass('disabled').removeAttr('data-toggle');
    $('button.showDelete').addClass('disabled').off('click');
    $('button.showAddFile').addClass('disabled').removeAttr('data-toggle');
    $('button.showAddPlaylist').addClass('disabled').removeAttr('data-toggle');
    return;
  }

  show = shows.shows[selected_show];
  showView = new main.ShowView(show);
  show.fetch_files();
  $('button.showNew').removeClass('disabled').attr('data-toggle', 'modal');
  $('button.showClone').removeClass('disabled').attr('data-toggle', 'modal');
  $('button.showAddFile').removeClass('disabled').attr('data-toggle', 'modal');
  $('button.showAddPlaylist').removeClass('disabled').attr('data-toggle', 'modal');

  $('button.showDelete').removeClass('disabled').off('click').on('click', function() {
    var modal = $('#showDeleteModal');
    modal.find('#showDeleteName').text(show.name);

    var showDeleteBtn = modal.find('button.showDelete');
    showDeleteBtn.off('click').on('click', function() {
      show.delete();
    });

    modal.modal('show');
  });
}

main.ShowListView.prototype.render_error = function() {
  $('#error-message').text(this.model.last_error)
  $('#error').show();
};

/****** show ******/

var showView = null;

main.ShowView = function(model) {
  this.model = model;
  this.$el = $('#hiddenTemplates .showTemplate').clone().removeClass('showTemplate');

  var self = this;
  var selectFiles = this.$el.find('a.showSelectFiles');
  var selectPlaylists = this.$el.find('a.showSelectPlaylists');
  var cardBody = self.$el.find('div.card-body');

  selectFiles.on('click', function() {
    selectFiles.addClass('active');
    selectPlaylists.removeClass('active');
    self.model.fetch_files();
    cardBody.text("... loading files ...");
  });
  selectPlaylists.on('click', function() {
    selectPlaylists.addClass('active');
    selectFiles.removeClass('active');
    self.model.fetch_playlists();
    cardBody.text("... loading playlists ...")
  });

  $(this.model).on('update-files', function() {
    self.render_files(cardBody);
  });
  $(this.model).on('update-playlists', function() {
    self.render_playlists(cardBody);
  });
  $(this.model).on('error', function() {
    self.render_error();
  });

  $('#showView').html(this.$el);
};

main.ShowView.prototype.render_files = function(body) {
  $('#error').hide();

  var fileListView = new main.FileListView(this.model.files, this);
  fileListView.render();
  body.html(fileListView.$el);
};

main.ShowView.prototype.render_playlists = function(body) {
  $('#error').hide();
  var fileListView = new main.PlaylistListView(this.model.playlists, this);
  fileListView.render();
  body.html(fileListView.$el);
};

main.ShowView.prototype.render_error = function() {
  $('#error-message').text(this.model.last_error)
  $('#error').show();
};

/****** files ******/

main.FileListView = function(model) {
  this.model = model;
  this.$el = $('#hiddenTemplates .filesTemplate').clone().removeClass('filesTemplate');
};

main.FileListView.prototype.render = function() {
  var table = this.$el.find("tbody");
  for (var i = 0; i < this.model.length; i++) {
    var fileView = new main.FileView(this.model[i], this);
    fileView.render();
    table.append(fileView.$el);
  }
};

main.FileView = function(model) {
  this.model = model;
  this.$el = $('#hiddenTemplates .fileTemplate tr').clone();
};

main.FileView.prototype.render = function() {
  this.$el.find('.fileId').text(this.model.id)
  this.$el.find('.fileArtist').text(this.model.metadata.artist)
  this.$el.find('.fileAlbum').text(this.model.metadata.album)
  this.$el.find('.fileTitle').text(this.model.metadata.title)
  this.$el.find('.fileDuration').text(format_duration(this.model.duration))

  var self = this;
  this.$el.find('.fileEdit').on('click', function() {
    self.render_edit_modal();
  });
  this.$el.find('.fileLogs').on('click', function() {
    self.model.fetch_logs();
  });
  this.$el.find('.fileDelete').on('click', function() {
    self.model.delete();
  });
};

main.FileView.prototype.render_edit_modal = function() {
  var modal = $('#fileEditModal');
  var fileEditBtn = modal.find('button.fileEdit');

  modal.find('#fileEditID').text(this.model.$show.name + '/' + this.model.id);
  var fileEditArtist =  modal.find('#fileEditArtist').val(this.model.metadata.artist);
  var fileEditTitle = modal.find('#fileEditTitle').val(this.model.metadata.title);
  var fileEditAlbum = modal.find('#fileEditAlbum').val(this.model.metadata.album);
  var fileEditOrganization = modal.find('#fileEditOrganization').val(this.model.metadata.organization);
  var fileEditISRC = modal.find('#fileEditISRC').val(this.model.metadata.isrc);

  var self = this;
  fileEditBtn.off('click').on('click', function() {
    var metadata = {};
    metadata.artist = fileEditArtist.val();
    metadata.title = fileEditTitle.val();
    metadata.album = fileEditAlbum.val();
    metadata.organization = fileEditOrganization.val();
    metadata.isrc = fileEditISRC.val();
    self.model.edit(metadata);
  });

  modal.find('#fileEditForm').off('submit').on('submit', function() {
    fileEditBtn.trigger('click');
    return false;
  });

  modal.on('shown.bs.modal', function(event) {
    fileEditArtist.focus();
  });

  modal.modal('show');
};

/****** playlists ******/

main.PlaylistListView = function(model) {
  this.model = model;
  this.$el = $('#hiddenTemplates .playlistsTemplate').clone().removeClass('playlistsTemplate');
};

main.PlaylistListView.prototype.render = function() {
  var table = this.$el.find("tbody");
  for (var i = 0; i < this.model.length; i++) {
    var playlistView = new main.PlaylistView(this.model[i], this);
    playlistView.render();
    table.append(playlistView.$el);
  }
};

main.PlaylistView = function(model) {
  this.model = model;
  this.$el = $('#hiddenTemplates .playlistTemplate tr').clone();
};

main.PlaylistView.prototype.render = function() {
  this.$el.find('.playlistId').text(this.model.id)
  this.$el.find('.playlistDescription').text(this.model.description)
  this.$el.find('.playlistLength').text(this.model.entries.length)
  this.$el.find('.playlistCreated').text(format_datetime(this.model.created))

  var self = this;
  // TODO: implement playlist edit
  this.$el.find('.playlistDelete').on('click', function() {
    self.model.delete();
  });
};

/***************** controller *****************/

function modal_show_new_init() {
  var modal = $('#showNewModal');
  var showNewName = modal.find('#showNewName');
  var showNewBtn = modal.find('button.showNew');

  showNewName.on('keyup', function() {
    if(showNewName.val().length == 0) {
      showNewBtn.prop('disabled', true)
    } else {
      showNewBtn.prop('disabled', false)
    }
  });

  modal.find('#showNewForm').on('submit', function() {
    showNewBtn.trigger('click');
    return false;
  });

  modal.on('show.bs.modal', function(event) {
    showNewName.val("");

    showNewBtn.prop('disabled', true)
    showNewBtn.off('click').on('click', function() {
      shows.new_show(showNewName.val());
    });
  });

  modal.on('shown.bs.modal', function(event) {
    showNewName.focus();
  });
}

function modal_show_clone_init() {
  var modal = $('#showCloneModal');
  var showCloneName = modal.find('#showCloneName');
  var showCloneBtn = modal.find('button.showClone');

  showCloneName.on('keyup', function() {
    if(showCloneName.val().length == 0) {
      showCloneBtn.prop('disabled', true)
    } else {
      showCloneBtn.prop('disabled', false)
    }
  });

  modal.find('#showCloneForm').on('submit', function() {
    showCloneBtn.trigger('click');
    return false;
  });

  modal.on('show.bs.modal', function(event) {
    showCloneName.val("");

    showCloneBtn.prop('disabled', true)
    showCloneBtn.off('click').on('click', function() {
      show.clone(showCloneName.val());
    });
  });

  modal.on('shown.bs.modal', function(event) {
    showCloneName.focus();
  });
}

function modal_file_add_init() {
  var modal = $('#fileAddModal');
  // var fileAddURI = modal.find('#fileAddURI');
  var fileAddSelect = modal.find('#fileAddSelect');
  var fileAddBtn = modal.find('button.fileAdd');

  // fileAddURI.on('keyup', function() {
  //   if(fileAddURI.val().length == 0) {
  //     fileAddBtn.prop('disabled', true)
  //   } else {
  //     fileAddBtn.prop('disabled', false)
  //   }
  // });

  // modal.find('#fileAddForm').on('submit', function() {
  //   fileAddBtn.trigger('click');
  //   return false;
  // });

  modal.on('show.bs.modal', function(event) {
    // fileAddURI.val("");
    fileAddBtn.prop('disabled', true)
  });

  modal.on('shown.bs.modal', function(event) {
    // fileAddURI.focus();
    fileAddSelect.on('change', function() {
      var $this = this;
      fileAddBtn.prop('disabled', false)
      fileAddBtn.off('click').on('click', function() {
        show.add_file_upload($this.files[0]);
      });
    });
  });
}

function main_init() {
  auth = new main.AuthInfo();
  authInfoView = new main.AuthInfoView(auth);
  shows = new main.ShowList();
  showListView = new main.ShowListView(shows);

  modal_show_new_init();
  modal_show_clone_init();
  modal_file_add_init();

  auth.init();
}

/*************************************************************************/
