//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//

package v1

import (
	"errors"

	"gitlab.servus.at/autoradio/tank/importer"
	"gitlab.servus.at/autoradio/tank/store"
)

// common
var (
	ErrNotFlowJSUpload            = errors.New("this is not a flow.js upload")
	ErrFlowJSChunkAlreadUploading = errors.New("chunk is already uploading or done")
	ErrFileVanished               = errors.New("the file vanished from the store while preparing the import")
)

type ErrorResponse struct {
	Error  string      `json:"error,omitempty"`
	Detail interface{} `json:"detail,omitempty"`
}

// Shows
type ShowsListing struct {
	Shows []store.Show `json:"results"`
}

// Imports
type JobsListing struct {
	Jobs []*importer.Job `json:"results"`
}

// Files
type FilesListing struct {
	Files []store.File `json:"results"`
}

type FileCreateRequest struct {
	SourceURI string `json:"sourceURI" enums:"upload://<filename>,https://<host>/<path>,file:///<path>"`
}

type FileUsageListing struct {
	Usage struct {
		Playlists []store.Playlist `json:"playlists"`
	} `json:"results"`
}

type FileImportLogs struct {
	Logs store.ImportLogs `json:"results"`
}

// Playlists
type PlaylistsListing struct {
	Playlists []store.Playlist `json:"results"`
}
