//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright  (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package v1

import (
	"io"
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.servus.at/autoradio/tank/auth"
	"gitlab.servus.at/autoradio/tank/importer"
	"gitlab.servus.at/autoradio/tank/store"
)

//	@title			AURA Tank API
//	@version		1.0
//	@description	Import & Playlist Daemon

//	@contact.name	API Support
//	@contact.url	https://gitlab.servus.at/autoradio/tank

//	@license.name	AGPLv3
//	@license.url	https://www.gnu.org/licenses/agpl-3.0

//	@securityDefinitions.apikey	ApiKeyAuth
//	@in							header
//	@name						Authorization

type API struct {
	store    *store.Store
	importer *importer.Importer
	infoLog  *log.Logger
	errLog   *log.Logger
	dbgLog   *log.Logger
}

func NewAPI(st *store.Store, im *importer.Importer, infoLog, errLog, dbgLog *log.Logger) (api *API) {
	if infoLog == nil {
		infoLog = log.New(io.Discard, "", 0)
	}
	if errLog == nil {
		errLog = log.New(io.Discard, "", 0)
	}
	if dbgLog == nil {
		dbgLog = log.New(io.Discard, "", 0)
	}

	api = &API{}
	api.store = st
	api.importer = im
	api.infoLog = infoLog
	api.errLog = errLog
	api.dbgLog = dbgLog
	return
}

func InstallHTTPHandler(r *gin.RouterGroup, st *store.Store, im *importer.Importer, infoLog, errLog, dbgLog *log.Logger) {
	r.Use(auth.Middleware())
	api := NewAPI(st, im, infoLog, errLog, dbgLog)

	// Shows
	shows := r.Group("shows")
	{
		shows.GET("", api.ListShows)
		shows.POST(":show-id", api.CreateShow)
		shows.DELETE(":show-id", api.DeleteShow)
		// deprecated
		shows.GET(":show-id/imports", api.ListImportsOfShow)

		// Show/Files
		files := shows.Group(":show-id/files")
		{
			// deprecated
			files.GET("", api.ListFilesOfShow)
			files.POST("", api.CreateFileForShow)
			files.GET(":file-id", api.ReadFileOfShow)
			files.PATCH(":file-id", api.PatchFileOfShow)
			files.DELETE(":file-id", api.DeleteFileOfShow)

			// deprecated
			files.GET(":file-id/usage", api.ReadUsageOfFile)

			// deprecated
			files.GET(":file-id/logs", api.ReadLogsOfFile)

			// deprecated
			files.GET(":file-id/import", api.ReadImportOfFile)
			files.DELETE(":file-id/import", api.CancelImportOfFile)

			// TODO: distignuish between flow.js and simple upload using the content type?!?
			// deprecated
			files.PUT(":file-id/upload", api.UploadFileSimple)
			files.POST(":file-id/upload", api.UploadFileFlowJS)
			files.GET(":file-id/upload", api.TestFileFlowJS)
		}

		// Show/Playlists
		playlists := shows.Group(":show-id/playlists")
		{
			// deprecated
			playlists.GET("", api.ListPlaylistsOfShow)
			playlists.POST("", api.CreatePlaylistForShow)
			playlists.GET(":playlist-id", api.ReadPlaylistOfShow)
			playlists.PUT(":playlist-id", api.UpdatePlaylistOfShow)
			playlists.DELETE(":playlist-id", api.DeletePlaylistOfShow)
		}
	}

	playlists := r.Group("playlists")
	{
		playlists.GET("", api.ListPlaylists) // behaves like ListPlaylistsOfShow with ?showName
		playlists.POST("", api.CreatePlaylistForShow)
		playlists.GET(":playlist-id", api.ReadPlaylist) // behaves like ReadPlaylistOfShow with ?showName
		playlists.PUT(":playlist-id", api.UpdatePlaylistOfShow)
		playlists.DELETE(":playlist-id", api.DeletePlaylistOfShow)
	}

	// FIXME: This should be done with r.Groups when nested routes are removed
	// r.Group("imports")
	r.GET("imports", api.ListImportsOfShow)

	// r.Group("files")
	r.GET("files", api.ListFilesOfShow)
	r.POST("files", api.CreateFileForShow)
	r.GET("files/:file-id", api.ReadFileOfShow)
	r.PATCH("files/:file-id", api.PatchFileOfShow)
	r.DELETE("files/:file-id", api.DeleteFileOfShow)

	// r.Group("usage")
	r.GET("usage/:file-id", api.ReadUsageOfFile)

	// r.Group("logs")
	r.GET("logs/:file-id", api.ReadLogsOfFile)

	// r.Group("import")
	r.GET("import/:file-id", api.ReadImportOfFile)
	r.DELETE("import/:file-id", api.CancelImportOfFile)

	// r.Group("upload")
	r.PUT("upload/:file-id", api.UploadFileSimple)
	r.POST("upload/:file-id", api.UploadFileFlowJS)
	r.GET("upload/:file-id", api.TestFileFlowJS)
}
