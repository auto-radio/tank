//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package v1

import (
	"net/http"
	"sort"

	"github.com/gin-gonic/gin"
	"gitlab.servus.at/autoradio/tank/store"
)

// ListShows returns a list of all shows that are accessible to the current session.
// If authentication is disabled a list of all existing shows is returned.
//	@Summary		List shows
//	@Description	Lists all existing shows
//	@Produce		json
//	@Param			limit	query		int	false	"Limit number of results"
//	@Param			offset	query		int	false	"Start listing from offset"
//	@Success		200		{object}	ShowsListing
//	@Failure		400		{object}	ErrorResponse
//	@Failure		500		{object}	ErrorResponse
//	@Router			/api/v1/shows [get]
func (api *API) ListShows(c *gin.Context) {
	offset, limit, ok := getPaginationParameter(c)
	if !ok {
		return
	}

	storeShows, err := api.store.ListShows()
	if err != nil {
		c.JSON(http.StatusInternalServerError, ErrorResponse{Error: err.Error()})
		return
	}

	s := getAuthSession(c.Request) // this will panic if there is no session
	showsMap := make(map[string]store.Show)
	for _, showName := range s.Shows {
		showsMap[showName] = store.Show{Name: showName}
	}
	for _, showName := range s.PublicShows {
		showsMap[showName] = store.Show{Name: showName}
	}
	for _, show := range storeShows {
		if _, ok := showsMap[show.Name]; ok || s.AllShows {
			showsMap[show.Name] = show
		}
	}

	shows := []store.Show{}
	for _, show := range showsMap {
		shows = append(shows, show)
	}
	sort.Slice(shows, func(i, j int) bool { return shows[i].Name < shows[j].Name })

	if offset > 0 {
		if offset >= len(shows) {
			c.JSON(http.StatusOK, ShowsListing{Shows: []store.Show{}})
			return
		}
		shows = shows[offset:]
	}
	if limit >= 0 && limit < len(shows) {
		shows = shows[:limit]
	}

	c.JSON(http.StatusOK, ShowsListing{Shows: shows})
}

// CreateShow creates a new show.
//	@Summary		Create show
//	@Description	Creates a new show
//	@Produce		json
//	@Param			showName	query		string	true	"Name of the show to be created"
//	@Param			cloneFrom	query		string	false	"If given, all files and playlists will be copied from the show"
//	@Success		201			{object}	store.Show
//	@Failure		403			{object}	ErrorResponse
//	@Failure		404			{object}	ErrorResponse
//	@Failure		409			{object}	ErrorResponse
//	@Failure		500			{object}	ErrorResponse
//	@Router			/api/v1/shows/ [post]
func (api *API) CreateShow(c *gin.Context) {
	showID := c.Param("show-id")
	if authorized, _ := authorizeRequestForShow(c, showID); !authorized {
		return
	}
	srcID := c.Query("cloneFrom")

	var show *store.Show
	var err error
	if srcID != "" {
		if authorized, _ := authorizeRequestForShow(c, srcID); !authorized {
			return
		}
		show, err = api.store.CloneShow(showID, srcID)
	} else {
		show, err = api.store.CreateShow(showID)
	}
	if err != nil {
		sendError(c, err)
		return
	}

	c.JSON(http.StatusCreated, show)
}

// DeleteShow deletes a show.
//	@Summary		Delete show
//	@Description	Deletes a show
//	@Produce		json
//	@Param			showName	path	string	true	"Name of the show to be deleted"
//	@Success		204
//	@Failure		403	{object}	ErrorResponse
//	@Failure		500	{object}	ErrorResponse
//	@Router			/api/v1/shows/{showName} [delete]
func (api *API) DeleteShow(c *gin.Context) {
	showID := c.Param("show-id")
	if authorized, s := authorizeRequestForShow(c, showID); !authorized {
		return
	} else if !s.Privileged {
		c.JSON(http.StatusForbidden, ErrorResponse{Error: "only privileged users are allowed to delete shows"})
		return
	}

	if err := api.store.DeleteShow(showID); err != nil {
		sendError(c, err)
		return
	}

	c.JSON(http.StatusNoContent, nil)
}
