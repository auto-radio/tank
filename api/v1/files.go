//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package v1

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.servus.at/autoradio/tank/importer"
	"gitlab.servus.at/autoradio/tank/store"
)

// ListFilesOfShow returns a list of all files of the show.
//	@Summary		List files
//	@Description	Lists files of show
//	@Produce		json
//	@Param			showName	query		string	true	"Name of the show"
//	@Param			limit		query		int		false	"Limit number of results"
//	@Param			offset		query		int		false	"Start listing from offset"
//	@Success		200			{object}	FilesListing
//	@Failure		400			{object}	ErrorResponse
//	@Failure		403			{object}	ErrorResponse
//	@Failure		500			{object}	ErrorResponse
//	@Router			/api/v1/files [get]
func (api *API) ListFilesOfShow(c *gin.Context) {
	showID := c.Param("show-id")
	if showID == "" {
		showID = c.Query("showName")
	}
	if authorized, _ := authorizeRequestForShow(c, showID); !authorized {
		return
	}
	offset, limit, ok := getPaginationParameter(c)
	if !ok {
		return
	}

	files, err := api.store.ListFiles(showID, offset, limit)
	if err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusOK, FilesListing{files})
}

// CreateFileForShow adds a file and starts import.
//	@Summary		Add file
//	@Description	Adds a file and starts import
//	@Accept			json
//	@Produce		json
//	@Param			showName	query		string				true	"Name of the show"
//	@Param			file		body		FileCreateRequest	true	"URI of the file"
//	@Param			waitFor		query		string				false	"running|done - If given, return not before import has the given state"
//	@Success		201			{object}	store.File
//	@Failure		400			{object}	ErrorResponse
//	@Failure		403			{object}	ErrorResponse
//	@Failure		500			{object}	ErrorResponse
//	@Router			/api/v1/files [post]
func (api *API) CreateFileForShow(c *gin.Context) {
	showID := c.Param("show-id")
	if showID == "" {
		showID = c.Query("showName")
	}
	authorized, sess := authorizeRequestForShow(c, showID)
	if !authorized {
		return
	}

	request := &FileCreateRequest{}
	err := json.NewDecoder(c.Request.Body).Decode(request)
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "error decoding request: " + err.Error()})
		return
	}
	if request.SourceURI == "" {
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "sourceURI is mandatory"})
		return
	}
	srcURI, err := api.importer.ParseAndVerifySourceURI(request.SourceURI)
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "sourceURI is invalid: " + err.Error()})
		return
	}
	waitFor := c.Query("waitFor")
	switch waitFor {
	case "running":
	case "done":
	case "":
	default:
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "unable to wait for unknown state: " + waitFor})
		return
	}

	file := &store.File{}
	file.Source.URI = request.SourceURI
	if file, err = api.store.CreateFile(showID, *file); err != nil {
		sendError(c, err)
		return
	}

	// From here on, in case of an error we should either delete the newly created
	// file or return the file object as ErrorResponse.Detail so the API user can deal with the problem.
	job, err := api.importer.CreateJob(showID, file.ID, srcURI, sess.Username, c.Query("refID"))
	if err != nil {
		goto create_file_response
	}

	if err = job.StartWithTimeout(0); err != nil {
		goto create_file_response
	}

	switch waitFor {
	case "running":
		<-job.Running()
	case "done":
		<-job.Done()
	}

create_file_response:
	// GetFile only fails if the file does not exist or there is
	// a problem with the database connection. The former probably
	// means that the file got deleted by somebody else... all other
	// errors most likely mean that the file still exists. In this
	// case it's better to return the file so the API user can use
	// the file ID for further inspection.
	getFile, getErr := api.store.GetFile(showID, file.ID)
	if getErr == store.ErrNotFound {
		sendError(c, ErrFileVanished)
		return
	}
	// don't mask the original error
	if err == nil {
		err = getErr
	}
	if getFile != nil {
		file = getFile
	}

	if err == nil {
		c.JSON(http.StatusCreated, file)
		return
	}
	status, errResp := statusCodeFromError(err)
	errResp.Detail = file
	c.JSON(status, errResp)
}

// ReadFileOfShow retrieves file object.
//	@Summary		Retrieve file
//	@Description	Retrieves file object.
//	@Produce		json
//	@Param			showName	query		string	true	"Name of the show"
//	@Param			id			path		int		true	"ID of the file"
//	@Success		200			{object}	store.File
//	@Failure		400			{object}	ErrorResponse
//	@Failure		403			{object}	ErrorResponse
//	@Failure		404			{object}	ErrorResponse
//	@Failure		500			{object}	ErrorResponse
//	@Router			/api/v1/files/{id} [get]
func (api *API) ReadFileOfShow(c *gin.Context) {
	showID := c.Param("show-id")
	if showID == "" {
		showID = c.Query("showName")
	}
	if authorized, _ := authorizeRequestForShow(c, showID); !authorized {
		return
	}

	id, err := idFromString(c.Param("file-id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "invalid file-id: " + err.Error()})
		return
	}
	file, err := api.store.GetFile(showID, id)
	if err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusOK, file)
}

// PatchFileOfShow updates file metadata.
//	@Summary		Update file
//	@Description	Updates file metadata.
//	@Accept			json
//	@Produce		json
//	@Param			showName	query		string				true	"Name of the show"
//	@Param			id			path		int					true	"ID of the file"
//	@Param			metadata	body		store.FileMetadata	false	"File metadata"
//	@Success		200			{object}	store.File
//	@Failure		400			{object}	ErrorResponse
//	@Failure		403			{object}	ErrorResponse
//	@Failure		404			{object}	ErrorResponse
//	@Failure		500			{object}	ErrorResponse
//	@Router			/api/v1/files/{id} [patch]
func (api *API) PatchFileOfShow(c *gin.Context) {
	showID := c.Param("show-id")
	if showID == "" {
		showID = c.Query("showName")
	}
	if authorized, _ := authorizeRequestForShow(c, showID); !authorized {
		return
	}

	id, err := idFromString(c.Param("file-id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "invalid file-id: " + err.Error()})
		return
	}
	data := make(map[string]string)
	if err = json.NewDecoder(c.Request.Body).Decode(&data); err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "error decoding request: " + err.Error()})
		return
	}
	file, err := api.store.UpdateFileMetadata(showID, id, data)
	if err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusOK, file)
}

// DeleteFileOfShow removes a file.
//	@Summary		Delete file
//	@Description	Removes a file.
//	@Param			showName	query	string	true	"Name of the show"
//	@Param			id			path	int		true	"ID of the file"
//	@Success		204
//	@Failure		400	{object}	ErrorResponse
//	@Failure		403	{object}	ErrorResponse
//	@Failure		404	{object}	ErrorResponse
//	@Failure		409	{object}	ErrorResponse
//	@Failure		500	{object}	ErrorResponse
//	@Router			/api/v1/files/{id} [delete]
func (api *API) DeleteFileOfShow(c *gin.Context) {
	showID := c.Param("show-id")
	if showID == "" {
		showID = c.Query("showName")
	}
	if authorized, _ := authorizeRequestForShow(c, showID); !authorized {
		return
	}

	id, err := idFromString(c.Param("file-id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "invalid file-id: " + err.Error()})
		return
	}
	if job, err := api.importer.GetJob(showID, id); err != importer.ErrNotFound {
		job.Cancel()
	}
	if err = api.store.DeleteFile(showID, id); err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusNoContent, nil)
}

// ReadUsageOfFile lists playlists referring to the file.
//	@Summary		List referring playlists
//	@Description	Lists playlists referring to the file.
//	@Produce		json
//	@Param			showName	query		string	true	"Name of the show"
//	@Param			id			path		int		true	"ID of the file"
//	@Success		200			{object}	FileUsageListing
//	@Failure		400			{object}	ErrorResponse
//	@Failure		403			{object}	ErrorResponse
//	@Failure		404			{object}	ErrorResponse
//	@Failure		500			{object}	ErrorResponse
//	@Router			/api/v1/usage/{id} [get]
func (api *API) ReadUsageOfFile(c *gin.Context) {
	showID := c.Param("show-id")
	if showID == "" {
		showID = c.Query("showName")
	}
	if authorized, _ := authorizeRequestForShow(c, showID); !authorized {
		return
	}

	id, err := idFromString(c.Param("file-id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "invalid file-id: " + err.Error()})
		return
	}
	result := FileUsageListing{}
	if result.Usage.Playlists, err = api.store.GetFileUsage(showID, id); err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusOK, result)
}

// ReadLogsOfFile retrieves import logs of the file.
//	@Summary		Retrieve import logs
//	@Description	Retrieves import logs of the file.
//	@Produce		json
//	@Param			showName	query		string	true	"Name of the show"
//	@Param			id			path		int		true	"ID of the file"
//	@Success		200			{object}	FileImportLogs
//	@Failure		400			{object}	ErrorResponse
//	@Failure		403			{object}	ErrorResponse
//	@Failure		404			{object}	ErrorResponse
//	@Failure		500			{object}	ErrorResponse
//	@Router			/api/v1/logs/{id} [get]
func (api *API) ReadLogsOfFile(c *gin.Context) {
	showID := c.Param("show-id")
	if showID == "" {
		showID = c.Query("showName")
	}
	if authorized, _ := authorizeRequestForShow(c, showID); !authorized {
		return
	}

	id, err := idFromString(c.Param("file-id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "invalid file-id: " + err.Error()})
		return
	}
	result := FileImportLogs{}
	if result.Logs, err = api.store.GetImportLogs(showID, id); err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusOK, result)
}
