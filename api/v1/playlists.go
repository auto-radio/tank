//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package v1

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.servus.at/autoradio/tank/store"
)

// ListPlaylistsOfShow lists playlists.
//
//	@Summary		List playlists
//	@Description	Lists playlists of show.
//	@Produce		json
//	@Param			showName	query		string	true	"Name of the show"
//	@Param			limit		query		int		false	"Limit number of results"
//	@Param			offset		query		int		false	"Start listing from offset"
//	@Success		200			{object}	PlaylistsListing
//	@Failure		400			{object}	ErrorResponse
//	@Failure		403			{object}	ErrorResponse
//	@Failure		500			{object}	ErrorResponse
//	@Router			/api/v1/playlists [get]
func (api *API) ListPlaylistsOfShow(c *gin.Context) {
	// TODO: remove together with nested routes
	showID := c.Param("show-id")
	if authorized, _ := authorizeRequestForShow(c, showID); !authorized {
		return
	}
	offset, limit, ok := getPaginationParameter(c)
	if !ok {
		return
	}

	playlists, err := api.store.ListPlaylists(showID, offset, limit)
	if err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusOK, PlaylistsListing{playlists})
}

// CreatePlaylistForShow creates a new playlist.
//
//	@Summary		Create playlist
//	@Description	Creates a new playlist for the show.
//	@Accept			json
//	@Produce		json
//	@Param			showName	query		string			true	"Name of the show"
//	@Param			playlist	body		store.Playlist	true	"Playlist data"
//	@Success		201			{object}	store.Playlist
//	@Failure		400			{object}	ErrorResponse
//	@Failure		403			{object}	ErrorResponse
//	@Failure		500			{object}	ErrorResponse
//	@Router			/api/v1/playlists [post]
func (api *API) CreatePlaylistForShow(c *gin.Context) {
	showID := c.Param("show-id")
	if showID == "" {
		showID = c.Query("showName")
	}
	if authorized, _ := authorizeRequestForShow(c, showID); !authorized {
		return
	}

	playlist := &store.Playlist{}
	err := json.NewDecoder(c.Request.Body).Decode(playlist)
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "error decoding playlist: " + err.Error()})
		return
	}
	if playlist, err = api.store.CreatePlaylist(showID, *playlist); err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusCreated, playlist)
}

// ReadPlaylistOfShow retrieves a playlist of a show.
//
//	@Summary		Retrieve playlist
//	@Description	Retrieves a playlist of a show.
//	@Produce		json
//	@Param			showName	query		string	true	"Name of the show"
//	@Param			id			path		int		true	"ID of the playlist"
//	@Success		200			{object}	store.Playlist
//	@Failure		400			{object}	ErrorResponse
//	@Failure		403			{object}	ErrorResponse
//	@Failure		404			{object}	ErrorResponse
//	@Failure		500			{object}	ErrorResponse
//	@Router			/api/v1/playlists/{id} [get]
//
// TODO: remove this together with nested routes
func (api *API) ReadPlaylistOfShow(c *gin.Context) {
	showID := c.Param("show-id")
	if showID == "" {
		showID = c.Query("showName")
	}
	if authorized, _ := authorizeRequestForShow(c, showID); !authorized {
		return
	}

	id, err := idFromString(c.Param("playlist-id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "invalid playlist-id: " + err.Error()})
		return
	}
	playlist, err := api.store.GetPlaylist(showID, id)
	if err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusOK, playlist)
}

// UpdatePlaylistOfShow updates a playlist of a show.
//
//	@Summary		Update playlist
//	@Description	Updates a playlist of a show.
//	@Accept			json
//	@Produce		json
//	@Param			showName	query		string			true	"Name of the show"
//	@Param			id			path		int				true	"ID of the playlist"
//	@Param			playlist	body		store.Playlist	true	"Playlist data"
//	@Success		200			{object}	store.Playlist
//	@Failure		400			{object}	ErrorResponse
//	@Failure		403			{object}	ErrorResponse
//	@Failure		404			{object}	ErrorResponse
//	@Failure		500			{object}	ErrorResponse
//	@Router			/api/v1/playlists/{id} [put]
func (api *API) UpdatePlaylistOfShow(c *gin.Context) {
	showID := c.Param("show-id")
	if showID == "" {
		showID = c.Query("showName")
	}
	if authorized, _ := authorizeRequestForShow(c, showID); !authorized {
		return
	}

	id, err := idFromString(c.Param("playlist-id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "invalid playlist-id: " + err.Error()})
		return
	}
	playlist := &store.Playlist{}
	if err = json.NewDecoder(c.Request.Body).Decode(playlist); err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "error decoding playlist: " + err.Error()})
		return
	}
	if playlist, err = api.store.UpdatePlaylist(showID, id, *playlist); err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusOK, playlist)
}

// DeletePlaylistOfShow deletes a playlist of a show.
//
//	@Summary		Delete playlist
//	@Description	Deletes a playlist of a show.
//	@Param			showName	query	string	true	"Name of the show"
//	@Param			id			path	int		true	"ID of the playlist"
//	@Success		204
//	@Failure		400	{object}	ErrorResponse
//	@Failure		403	{object}	ErrorResponse
//	@Failure		404	{object}	ErrorResponse
//	@Failure		500	{object}	ErrorResponse
//	@Router			/api/v1/playlists/{id} [delete]
func (api *API) DeletePlaylistOfShow(c *gin.Context) {
	showID := c.Param("show-id")
	if showID == "" {
		showID = c.Query("showName")
	}
	if authorized, _ := authorizeRequestForShow(c, showID); !authorized {
		return
	}

	id, err := idFromString(c.Param("playlist-id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "invalid playlist-id: " + err.Error()})
		return
	}
	if err = api.store.DeletePlaylist(showID, id); err != nil {
		sendError(c, err)
		return
	}
	c.JSON(http.StatusNoContent, nil)
}

// global

// ListPlaylists lists all playlists.
//
//	@Summary		List playlists
//	@Description	Lists all playlists.
//	@Produce		json
//	@Param			showName	query		string	false	"Name of the show"
//	@Param			limit		query		int		false	"Limit number of results"
//	@Param			offset		query		int		false	"Start listing from offset"
//	@Success		200			{object}	PlaylistsListing
//	@Failure		400			{object}	ErrorResponse
//	@Failure		403			{object}	ErrorResponse
//	@Failure		500			{object}	ErrorResponse
//	@Router			/api/v1/playlists [get]
func (api *API) ListPlaylists(c *gin.Context) {
	if authorized, _ := authorizeRequestAllShows(c); !authorized {
		return
	}
	offset, limit, ok := getPaginationParameter(c)
	if !ok {
		return
	}
	if showID := c.Query("showName"); showID != "" {
		playlists, err := api.store.ListPlaylists(showID, offset, limit)
		if err != nil {
			sendError(c, err)
			return
		}
		c.JSON(http.StatusOK, PlaylistsListing{playlists})
	} else {
		playlists, err := api.store.ListPlaylistsAllShows(offset, limit)
		if err != nil {
			sendError(c, err)
			return
		}
		c.JSON(http.StatusOK, PlaylistsListing{playlists})
	}
}

// ReadPlaylist retrieves a playlist.
//
//	@Summary		Retrieve playlist
//	@Description	Retrieves a playlist.
//	@Produce		json
//	@Param			showName	query		string	false	"Name of the show"
//	@Param			id			path		int		true	"ID of the playlist"
//	@Success		200			{object}	store.Playlist
//	@Failure		400			{object}	ErrorResponse
//	@Failure		403			{object}	ErrorResponse
//	@Failure		404			{object}	ErrorResponse
//	@Failure		500			{object}	ErrorResponse
//	@Router			/api/v1/playlists/{id} [get]
func (api *API) ReadPlaylist(c *gin.Context) {
	if authorized, _ := authorizeRequestAllShows(c); !authorized {
		return
	}

	id, err := idFromString(c.Param("playlist-id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse{Error: "invalid playlist-id: " + err.Error()})
		return
	}
	if showID := c.Query("showName"); showID != "" {
		playlist, err := api.store.GetPlaylist(showID, id)
		if err != nil {
			sendError(c, err)
			return
		}
		c.JSON(http.StatusOK, playlist)
	} else {
		playlist, err := api.store.GetPlaylistAllShows(id)
		if err != nil {
			sendError(c, err)
			return
		}
		c.JSON(http.StatusOK, playlist)
	}
}
