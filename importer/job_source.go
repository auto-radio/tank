//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package importer

import (
	"context"
	"errors"
	"io"
	"mime"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"sync/atomic"
	"time"
)

func (job *Job) initializeSource() (err error) {
	if job.Source.Scheme == SourceSchemeUpload {
		// the source will be attached using job.AttachUploader() so all we need to do
		// is to wait for it to happen
		select {
		case <-job.Ctx.Done():
			return job.Ctx.Err()
		case <-job.subC.sourceAttached:
		}
		return
	}

	atomic.StoreUint32(&job.sourceSet, 1)

	switch job.Source.Scheme {
	case SourceSchemeHTTP:
		fallthrough
	case SourceSchemeHTTPs:
		job.source, err = newJobSourceHTTP(job.im.httpC, job.Source, job.Ctx)
	case SourceSchemeFake:
		job.source = newJobSourceFake(job.Source)
	case SourceSchemeFile:
		job.source, err = newJobSourceFile(job.Source, job.Ctx)
	default:
		// Naughty you! You should have used importer.ParseAndVerifySourceURI which would have
		// told you right away that this uri scheme is not supported
		return ErrSourceNotSupported
	}

	close(job.subC.sourceAttached)
	return
}

//******* fake

type JobSourceFake uint64

func newJobSourceFake(srcURL SourceURL) *JobSourceFake {
	// TODO: parse size from srcURL
	src := JobSourceFake(10 * 1024 * 1024) // simulate a 10 MB file...
	return &src
}

func (src *JobSourceFake) Len() uint64 {
	return uint64(*src)
}

func (src *JobSourceFake) String() string {
	return "fake source"
}

func (src *JobSourceFake) Read(p []byte) (n int, err error) {
	l := len(p)
	if *src > JobSourceFake(l) {
		*src = *src - JobSourceFake(l)
		time.Sleep(200 * time.Millisecond)
		return l, nil
	}
	l = int(*src)
	*src = 0
	return l, io.EOF
}

func (src *JobSourceFake) Done(result *JobSourceResult) {
	return
}

//******* Upload

type JobSourceUpload struct {
	len  uint64
	name string
	r    io.Reader
	done chan *JobSourceResult
}

func newJobSourceUpload(len uint64, name string, r io.Reader) *JobSourceUpload {
	src := &JobSourceUpload{len: len, name: name, r: r}
	src.done = make(chan *JobSourceResult, 1)
	return src
}

func (src *JobSourceUpload) Len() uint64 {
	return src.len
}

func (src *JobSourceUpload) String() string {
	return src.name
}

func (src *JobSourceUpload) Read(p []byte) (n int, err error) {
	return src.r.Read(p)
}

func (src *JobSourceUpload) Done(result *JobSourceResult) {
	if result == nil {
		close(src.done)
		return
	}
	src.done <- result
}

//******* http(s)

type JobSourceHTTP struct {
	r        *http.Response
	filename string
}

func newJobSourceHTTP(client *http.Client, srcURL SourceURL, ctx context.Context) (*JobSourceHTTP, error) {
	src := &JobSourceHTTP{}

	req, err := http.NewRequest(http.MethodGet, srcURL.String(), nil)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	// TODO: make user-agent configurable...
	req.Header.Set("User-Agent", "AURA tank 0.1")

	if src.r, err = client.Do(req); err != nil {
		return nil, err
	}
	if src.r.StatusCode != http.StatusOK {
		err = errors.New("server returned unexpected status: " + src.r.Status)
	}

	if mediatype, params, err := mime.ParseMediaType(src.r.Header.Get("Content-Disposition")); err == nil {
		if mediatype == "attachment" {
			src.filename = params["filename"]
		}
	}
	if src.filename == "" {
		_, src.filename = path.Split(srcURL.Path)
	}
	return src, err
}

func (src *JobSourceHTTP) Len() uint64 {
	cl := src.r.ContentLength
	if cl < 0 {
		return 0
	}
	return uint64(cl)
}

func (src *JobSourceHTTP) String() string {
	return "HTTP downloader: " + src.filename
}

func (src *JobSourceHTTP) Read(p []byte) (n int, err error) {
	return src.r.Body.Read(p)
}

func (src *JobSourceHTTP) Done(result *JobSourceResult) {
	_ = src.r.Body.Close() // dear linter: we are deliberately not handling errors from Close(), this is only called to free up resources
	return
}

//******* file

type JobSourceFile struct {
	f    *os.File
	size uint64
}

func newJobSourceFile(srcURL SourceURL, ctx context.Context) (*JobSourceFile, error) {
	f, err := os.Open(filepath.FromSlash(srcURL.Path))
	if err != nil {
		return nil, err
	}
	if deadline, ok := ctx.Deadline(); ok {
		_ = f.SetDeadline(deadline) // this will probably fail but we try it anyway
	}

	fi, err := f.Stat()
	if err != nil {
		_ = f.Close() // dear linter: we are deliberately not handling errors from Close(), this is only called to free up resources
		return nil, err
	}
	if !fi.Mode().IsRegular() {
		_ = f.Close() // dear linter: we are deliberately not handling errors from Close(), this is only called to free up resources
		return nil, errors.New("opening '" + srcURL.Path + "' failed: not a regular file")
	}
	if fi.Size() <= 0 {
		_ = f.Close() // dear linter: we are deliberately not handling errors from Close(), this is only called to free up resources
		return nil, errors.New("opening '" + srcURL.Path + "' failed: file-size is zero or negativ")
	}

	src := &JobSourceFile{}
	src.f = f
	src.size = uint64(fi.Size())
	return src, nil
}

func (src *JobSourceFile) Len() uint64 {
	return src.size
}

func (src *JobSourceFile) String() string {
	return "local file: " + src.f.Name()
}

func (src *JobSourceFile) Read(p []byte) (n int, err error) {
	return src.f.Read(p)
}

func (src *JobSourceFile) Done(result *JobSourceResult) {
	_ = src.f.Close() // dear linter: we are deliberately not handling errors from Close(), this is only called to free up resources
	return
}
