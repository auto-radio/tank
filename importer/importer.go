//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package importer

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"runtime"
	"sync"
	"time"

	"gitlab.servus.at/autoradio/tank/store"
)

type Importer struct {
	conf     *Config
	infoLog  *log.Logger
	errLog   *log.Logger
	dbgLog   *log.Logger
	store    *store.Store
	wgWorker sync.WaitGroup
	workers  []*WorkerState
	work     chan *Job
	jobs     *jobInventory
	httpC    *http.Client
}

func (im *Importer) ListJobs(show string, offset, limit int) ([]*Job, error) {
	return im.jobs.ListJobs(show, offset, limit), nil // for now error is always nil but this might change later
}

func (im *Importer) ParseAndVerifySourceURI(uri string) (*SourceURL, error) {
	src, err := url.Parse(uri)
	if err != nil {
		return nil, err
	}

	switch src.Scheme {
	case SourceSchemeHTTP:
	case SourceSchemeHTTPs:
	case SourceSchemeFake:
	case SourceSchemeFile:
	case SourceSchemeUpload:
	default:
		return nil, ErrSourceNotSupported
	}

	return (*SourceURL)(src), nil
}

func (im *Importer) CreateJob(show string, id uint64, src *SourceURL, user, refID string) (*Job, error) {
	_, err := im.store.SetFileImportStateInitializing(show, id)
	if err != nil {
		return nil, err
	}

	return im.jobs.GetOrNewJob(show, id, im, src, user, refID), nil
}

func (im *Importer) GetJob(show string, id uint64) (*Job, error) {
	return im.jobs.GetJob(show, id)
}

func (im *Importer) Healthz(ctx context.Context) (err error) {
	numOK := 0
	for _, ws := range im.workers {
		switch ws.get() {
		case WorkerIdle:
			fallthrough
		case WorkerBusy:
			numOK++
		}
	}
	if numOK != im.conf.Workers {
		err = fmt.Errorf("only %d out of %d workers are running", numOK, im.conf.Workers)
	}
	return
}

// handle subscriptions to new jobs-of-show -> pass on to job inventory

func (im *Importer) deleteJob(show string, id uint64) error {
	return im.jobs.DeleteJob(show, id)
}

func (im *Importer) runWorker(idx int, state *WorkerState) {
	defer im.dbgLog.Printf("importer: worker(%d) has stopped", idx)
	defer state.set(WorkerStopped)

	im.dbgLog.Printf("importer: worker(%d) is running", idx)
	state.set(WorkerIdle)
	for job := range im.work {
		state.set(WorkerBusy)
		im.infoLog.Printf("importer: worker(%d) starting job(%s/%d) ...", idx, job.Show, job.ID)

		// job.run() will take care of cancelation and timeouts to make sure this function returns in time.
		// In case the job is already canceled we still need to run this to make sure all resources
		// allocated by the job are properly cleaned up and the job is removed from the inventory.
		if err := job.run(); err != nil {
			if err == context.Canceled {
				im.infoLog.Printf("importer: worker(%d) job(%s/%d) was canceled", idx, job.Show, job.ID)
			} else {
				im.errLog.Printf("importer: worker(%d) job(%s/%d) failed: %s", idx, job.Show, job.ID, err.Error())
			}
		} else {
			im.infoLog.Printf("importer: worker(%d) successfully completed job(%s/%d)", idx, job.Show, job.ID)
		}

		state.set(WorkerIdle)
	}
}

func NewImporter(conf Config, st *store.Store, infoLog, errLog, dbgLog *log.Logger) (im *Importer, err error) {
	if infoLog == nil {
		infoLog = log.New(io.Discard, "", 0)
	}
	if errLog == nil {
		errLog = log.New(io.Discard, "", 0)
	}
	if dbgLog == nil {
		dbgLog = log.New(io.Discard, "", 0)
	}

	im = &Importer{conf: &conf, infoLog: infoLog, errLog: errLog, dbgLog: dbgLog, store: st}

	if im.conf.JobTimeout <= 0 {
		im.conf.JobTimeout = 3 * time.Hour
	}
	if im.conf.Workers <= 0 {
		im.conf.Workers = runtime.NumCPU()
	}
	if im.conf.Backlog == 0 {
		im.conf.Backlog = DefaultBacklog
	}

	im.work = make(chan *Job, im.conf.Backlog)
	for i := 0; i < im.conf.Workers; i = i + 1 {
		im.wgWorker.Add(1)
		go func(idx int) {
			defer im.wgWorker.Done()
			state := WorkerNew
			im.workers = append(im.workers, &state)
			im.runWorker(idx, &state)
		}(i)
	}
	im.jobs = newJobInventory(infoLog, errLog, dbgLog)
	im.httpC = &http.Client{}

	infoLog.Printf("importer: started with %d worker", im.conf.Workers)
	return
}
