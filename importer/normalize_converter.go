//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package importer

import (
	"errors"
	"fmt"
	"io"
	"os/exec"
	"strconv"
	"syscall"

	"gitlab.servus.at/autoradio/tank/store"
)

type normalizeConverter interface {
	io.WriteCloser
	Wait() (log *store.Log, err error)
}

func (job *Job) newNormalizeConverter(loudness interface{}) (normalizeConverter, error) {
	if loudness == nil {
		// make default converter configurable?
		// mind that there is no nullNormalizeConverter because we need something to
		// convert incoming files to the format the store uses.
		return newFFmpegNormalizeConverter(job, nil)
	}

	switch loudness.(type) {
	case *ffmpegLoudnormParams:
		return newFFmpegNormalizeConverter(job, loudness.(*ffmpegLoudnormParams))
	}

	return nil, fmt.Errorf("unknown loudness parameter type: %T", loudness)
}

//******* FFmpeg

type ffmpegNormalizeConverter struct {
	job    *Job
	log    *store.Log
	cmd    *exec.Cmd
	stdin  io.WriteCloser
	stdout *convLogger
	stderr *convLogger
}

func newFFmpegNormalizeConverter(job *Job, params *ffmpegLoudnormParams) (c *ffmpegNormalizeConverter, err error) {
	c = &ffmpegNormalizeConverter{job: job}
	c.log = &store.Log{}
	filename := job.im.store.GetFilePath(job.Show, job.ID)
	job.im.dbgLog.Printf("ffmpeg-converter: starting ffmpeg for file '%s'", filename)

	// c.cmd = exec.CommandContext(job.Ctx, "ffmpeg", "-hide_banner", "-nostats", "-i", "-")
	c.cmd = exec.CommandContext(job.Ctx, "ffmpeg", "-nostats", "-y", "-i", "-")
	c.cmd.Args = append(c.cmd.Args, "-map_metadata", "0", "-vn")
	if params != nil {
		// loudness normalization, see: http://k.ylo.ph/2016/04/04/loudnorm.html
		params_encoded := fmt.Sprintf("measured_I=%s:measured_LRA=%s:measured_TP=%s:measured_thresh=%s:offset=%s",
			params.InputI, params.InputLRA, params.InputTP, params.InputThresh, params.TargetOffset)
		c.cmd.Args = append(c.cmd.Args, "-filter:a", "loudnorm="+params_encoded+":print_format=summary:dual_mono=true")
	}
	c.cmd.Args = append(c.cmd.Args, "-ar", strconv.FormatUint(uint64(job.im.store.Audio.SampleRate), 10))
	c.cmd.Args = append(c.cmd.Args, "-f", job.im.store.Audio.Format.String(), filename)

	if c.stdin, err = c.cmd.StdinPipe(); err != nil {
		c.log.Append("stderr", "ERROR opening stdin pipe: "+err.Error())
		return nil, errors.New("failed to open stdin pipe for ffmpeg: " + err.Error())
	}

	var stdout, stderr io.Reader
	if stdout, err = c.cmd.StdoutPipe(); err != nil {
		c.log.Append("stderr", "ERROR opening stdout pipe: "+err.Error())
		return nil, errors.New("failed to open stdout pipe for ffmpeg: " + err.Error())
	}
	c.stdout = newConvLogger(c.log, "stdout(ffmpeg)", stdout)
	if stderr, err = c.cmd.StderrPipe(); err != nil {
		c.log.Append("stderr", "ERROR opening stderr pipe: "+err.Error())
		return nil, errors.New("failed to open stderr pipe for ffmpeg: " + err.Error())
	}
	c.stderr = newConvLogger(c.log, "stderr(ffmpeg)", stderr)

	if err = c.cmd.Start(); err != nil {
		c.log.Append("stderr", "ERROR starting ffmpeg: "+err.Error())
		return nil, errors.New("failed to start ffmpeg: " + err.Error())
	}
	return
}

func (c *ffmpegNormalizeConverter) Write(p []byte) (n int, err error) {
	return c.stdin.Write(p)
}

func (c *ffmpegNormalizeConverter) Close() (err error) {
	return c.stdin.Close()
}

func (c *ffmpegNormalizeConverter) Wait() (log *store.Log, err error) {
	c.stdout.Wait()
	c.stderr.Wait()

	if err = c.cmd.Wait(); err != nil {
		if exiterr, ok := err.(*exec.ExitError); ok {
			if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
				info := fmt.Sprintf("ffmpeg returned %d", status.ExitStatus())
				c.job.im.dbgLog.Printf("ffmpeg-converter: %s", info)
				c.log.Append("stdout", info)
				if err != nil {
					err = errors.New(info)
				}
				return c.log, err
			}
		}
		c.job.im.errLog.Println("ffmpeg-converter: getting exit code of ffmpeg failed:", err)
		c.log.Append("stderr", "ERROR getting exit code of ffmpeg: "+err.Error())
		return c.log, errors.New("failed get exit code of ffmpeg: " + err.Error())
	}

	c.job.im.dbgLog.Println("ffmpeg-converter: ffmpeg returned 0 (success)")
	c.log.Append("stdout", "ffmpeg returned 0 (success)")
	return c.log, err
}
