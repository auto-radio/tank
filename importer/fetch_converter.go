//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package importer

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"syscall"

	"gitlab.servus.at/autoradio/tank/store"
)

type fetchConverter interface {
	io.WriteCloser
	Wait() (interface{}, *store.Log, error)
}

func (job *Job) newFetchConverter() (fetchConverter, error) {
	switch job.im.conf.Normalizer {
	case NormNone:
		return newNullFetchConverter(job)
	case NormFFmpeg:
		return newFFmpegFetchConverter(job)
	}
	panic("invalid normalizer")
}

//******* null

type nullFetchConverter struct {
	job  *Job
	file *os.File
	log  *store.Log
}

func newNullFetchConverter(job *Job) (c *nullFetchConverter, err error) {
	c = &nullFetchConverter{job: job}
	c.log = &store.Log{}
	filename := filepath.Join(job.WorkDir, "source")
	job.im.dbgLog.Printf("null-converter: opening file '%s'", filename)
	if c.file, err = os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0600); err != nil {
		c.log.Append("stderr", "ERROR opening file failed: "+err.Error())
		return
	}
	if deadline, ok := job.Ctx.Deadline(); ok {
		_ = c.file.SetDeadline(deadline) // this will probably fail but we try it anyway
	}
	c.log.Append("stdout", "successfully opened file: "+filename)
	return
}

func (c *nullFetchConverter) Write(p []byte) (n int, err error) {
	return c.file.Write(p)
}

func (c *nullFetchConverter) Close() (err error) {
	filename := c.file.Name()
	if err = c.file.Close(); err != nil {
		c.log.Append("stderr", "ERROR closing file failed: "+err.Error())
	} else {
		c.log.Append("stdout", "successfully closed file: "+filename)
	}
	return
}

func (c *nullFetchConverter) Wait() (loudness interface{}, log *store.Log, err error) {
	return nil, c.log, nil
}

//******* FFmpeg

type ffmpegFetchConverter struct {
	job    *Job
	log    *store.Log
	cmd    *exec.Cmd
	stdin  io.WriteCloser
	stdout *convLogger
	stderr *convLogger
}

func newFFmpegFetchConverter(job *Job) (c *ffmpegFetchConverter, err error) {
	c = &ffmpegFetchConverter{job: job}
	c.log = &store.Log{}
	filename := filepath.Join(job.WorkDir, "source")
	job.im.dbgLog.Printf("ffmpeg-converter: starting ffmpeg for file '%s'", filename)

	// c.cmd = exec.CommandContext(job.Ctx, "ffmpeg", "-hide_banner", "-nostats", "-i", "-")
	c.cmd = exec.CommandContext(job.Ctx, "ffmpeg", "-nostats", "-y", "-i", "-")
	c.cmd.Args = append(c.cmd.Args, "-map_metadata", "0", "-vn")
	c.cmd.Args = append(c.cmd.Args, "-codec:a", "pcm_s24le")
	c.cmd.Args = append(c.cmd.Args, "-f", "wav", filename)
	// loudness normalization, see: http://k.ylo.ph/2016/04/04/loudnorm.html
	c.cmd.Args = append(c.cmd.Args, "-map_metadata", "-1", "-vn")
	c.cmd.Args = append(c.cmd.Args, "-filter:a", "loudnorm=print_format=json:dual_mono=true")
	c.cmd.Args = append(c.cmd.Args, "-f", "null", "/dev/null")

	if c.stdin, err = c.cmd.StdinPipe(); err != nil {
		c.log.Append("stderr", "ERROR opening stdin pipe: "+err.Error())
		return nil, errors.New("failed to open stdin pipe for ffmpeg: " + err.Error())
	}

	var stdout, stderr io.Reader
	if stdout, err = c.cmd.StdoutPipe(); err != nil {
		c.log.Append("stderr", "ERROR opening stdout pipe: "+err.Error())
		return nil, errors.New("failed to open stdout pipe for ffmpeg: " + err.Error())
	}
	c.stdout = newConvLogger(c.log, "stdout(ffmpeg)", stdout)
	if stderr, err = c.cmd.StderrPipe(); err != nil {
		c.log.Append("stderr", "ERROR opening stderr pipe: "+err.Error())
		return nil, errors.New("failed to open stderr pipe for ffmpeg: " + err.Error())
	}
	c.stderr = newConvLogger(c.log, "stderr(ffmpeg)", stderr)

	if err = c.cmd.Start(); err != nil {
		c.log.Append("stderr", "ERROR starting ffmpeg: "+err.Error())
		return nil, errors.New("failed to start ffmpeg: " + err.Error())
	}
	return
}

func (c *ffmpegFetchConverter) Write(p []byte) (n int, err error) {
	return c.stdin.Write(p)
}

func (c *ffmpegFetchConverter) Close() (err error) {
	return c.stdin.Close()
}

func (c *ffmpegFetchConverter) fetchLoudnormParams() (*ffmpegLoudnormParams, error) {
	// the loudnorm filter posts its result onto stderr
	r := c.log.NewReader("stderr(ffmpeg)")

	// we are looking for a line starting with "[Parsed_loudnorm_0 " so 32 bytes is enough
	var buf [32]byte
	for {
		p := buf[:]
		n, err := r.Read(p) // this will always return a single, possibly truncated, line
		if err != nil {
			if err == io.EOF {
				err = errors.New("couldn't find any loudness parameters in ffmpeg output")
			}
			return nil, err
		}
		if strings.HasPrefix(string(p[:n]), "[Parsed_loudnorm_0 ") {
			break
		}
	}

	params := &ffmpegLoudnormParams{}
	jd := json.NewDecoder(r)
	jd.DisallowUnknownFields()
	if err := jd.Decode(&params); err != nil {
		return nil, err
	}
	return params, nil
}

func (c *ffmpegFetchConverter) Wait() (loudness interface{}, log *store.Log, err error) {
	c.stdout.Wait()
	c.stderr.Wait()

	if err = c.cmd.Wait(); err != nil {
		if exiterr, ok := err.(*exec.ExitError); ok {
			if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
				info := fmt.Sprintf("ffmpeg returned %d", status.ExitStatus())
				c.job.im.dbgLog.Printf("ffmpeg-converter: %s", info)
				c.log.Append("stdout", info)
				if err != nil {
					err = errors.New(info)
				}
				return nil, c.log, err
			}
		}
		c.job.im.errLog.Println("ffmpeg-converter: failed get exit code of ffmpeg:", err)
		c.log.Append("stderr", "ERROR failed get exit code of ffmpeg: "+err.Error())
		return nil, c.log, errors.New("failed get exit code of ffmpeg: " + err.Error())
	}

	c.job.im.dbgLog.Println("ffmpeg-converter: ffmpeg returned 0 (success)")
	c.log.Append("stdout", "ffmpeg returned 0 (success)")

	if loudness, err = c.fetchLoudnormParams(); err != nil {
		c.log.Append("stderr", "ERROR fetching parameters from ffmpeg loudnorm filter: "+err.Error())
		err = errors.New("failed to parse output of loudnorm filter: " + err.Error())
	}
	return loudness, c.log, err
}
