//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package importer

import (
	"crypto/sha256"
	//"encoding/base64"
	"encoding/hex"
	"io"
	// TODO: make hashing algo configurable
	//	"golang.org/x/crypto/blake2b"
)

type copyFromSourceResult struct {
	err  error
	hash string
}

func (job *Job) copyFromSource(w io.Writer, done chan<- copyFromSourceResult) {
	defer close(done)

	// hash, err := blake2b.New256(nil)
	// if err != nil {
	// 	panic("creating hash function failed: " + err.Error())
	// }
	hash := sha256.New()
	src := io.TeeReader(job.source, hash)
	written, err := io.Copy(&progressWriter{job, StepFetching, job.source.Len(), 0, w}, src)
	if err != nil {
		done <- copyFromSourceResult{err, ""}
		return
	}
	//hashStr := "blake2b_256:" + base64.URLEncoding.EncodeToString(hash.Sum(nil))
	hashStr := "sha256:" + hex.EncodeToString(hash.Sum(nil))
	job.im.dbgLog.Printf("fetch(): done copying %d bytes from source (%s)", written, hashStr)
	_, err = job.im.store.UpdateFileSourceHash(job.Show, job.ID, hashStr)
	done <- copyFromSourceResult{err, hashStr}
}

func (job *Job) fetch() (interface{}, error) {
	job.Progress.set(StepFetching, 0)

	// make sure a potentially connected source gets notified in any case
	defer job.source.Done(nil)

	conv, err := job.newFetchConverter()
	if err != nil {
		job.im.errLog.Printf("fetch(): creating fetch converter failed: %v", err)
		return nil, err
	}
	// from here on conv.Close() and conv.Wait() has to be called in any case to
	// reap potential child process zombies

	done := make(chan copyFromSourceResult)
	go job.copyFromSource(conv, done)

	var res copyFromSourceResult
	select {
	case <-job.Ctx.Done():
		_ = conv.Close() // dear linter: we are deliberately not handling errors from Close(), this is only called to free up resources
		// also any error given here would potentially hide errors reported from job.Ctx.Err() below which will contain much more useful information
		go conv.Wait() // do the zombie reaping in seperate go routine since we are not interested in the result anyway
		err = job.Ctx.Err()
		job.source.Done(&JobSourceResult{Err: err})
		return nil, err
	case res = <-done:
	}

	_ = conv.Close() // dear linter: we are deliberately not handling errors from Close(), this is only called to free up resources
	loudness, convLog, err := conv.Wait()
	job.im.dbgLog.Printf("fetch(): converter returned: %T, %v", loudness, err)
	if res.err != nil {
		err = res.err
	}

	if err := job.im.store.AddImportLog(job.Show, job.ID, "fetch", convLog); err != nil {
		job.im.infoLog.Printf("fetch(): failed to add fetch log output to store: %v", err)
	}
	// TODO: also write convLog to dbgLog?

	job.Progress.set(StepFetching, 1)
	job.source.Done(&JobSourceResult{err, res.hash, convLog})
	return loudness, err
}
