//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package importer

import (
	"context"
	"io"
	"os"
	"sync/atomic"
	"time"
)

// Job represents a import job for the file identified by Show and ID
type Job struct {
	im        *Importer
	Ctx       context.Context `json:"-"`
	cancel    context.CancelFunc
	State     JobState    `json:"state"`
	CreatedAt Timestamp   `json:"created" swaggertype:"string"`
	StartedAt Timestamp   `json:"started,omitempty"`
	ID        uint64      `json:"id"`
	Show      string      `json:"show"`
	User      string      `json:"user"`
	Source    SourceURL   `json:"source"`
	RefID     string      `json:"refID,omitempty"`
	Progress  JobProgress `json:"progress"`
	source    JobSource
	sourceSet uint32
	WorkDir   string `json:"-"`
	subC      struct {
		sourceAttached chan struct{}
		running        chan struct{}
		done           chan struct{}
	}
}

func (job *Job) run() error {
	// this function must never-ever hang or we block a worker forever - watch your context kids!!

	// when this function returns the job is either done or aborted...
	// either way job.cancel() needs to be called and the job needs to be removed from the inventory
	defer job.cancel()
	defer job.cleanup()
	defer close(job.subC.done)

	if !atomic.CompareAndSwapUint32((*uint32)(&job.State), uint32(JobPending), uint32(JobRunning)) {
		_, _ = job.im.store.SetFileImportStateAborted(job.Show, job.ID, "canceled by user or timeout") // dear linter: we are not interested in any errors from this function
		// the job was canceled before job.Start() could initialize the context and because of this
		// job.Cancel() only set the state to JobCanceled... so we simulate a canceled context here
		return context.Canceled
	}
	job.StartedAt.set(time.Now())
	job.im.dbgLog.Printf("running import for %s/%d from: %s", job.Show, job.ID, job.Source.String())
	_, _ = job.im.store.SetFileImportStateRunning(job.Show, job.ID) // dear linter: we are not interested in any errors from this function
	close(job.subC.running)

	if err := job.initializeSource(); err != nil {
		_, _ = job.im.store.SetFileImportStateAborted(job.Show, job.ID, "failed to initialize source: "+err.Error()) // dear linter: we are not interested in any errors from this function
		return err
	}
	// job.source is now initialized and points to a valid source
	job.im.dbgLog.Printf("job source is ready, fetching %d bytes from %s", job.source.Len(), job.source.String())
	loudness, err := job.fetch()
	if err != nil {
		_, _ = job.im.store.SetFileImportStateAborted(job.Show, job.ID, "fetching file failed: "+err.Error()) // dear linter: we are not interested in any errors from this function
		return err
	}
	if err = job.normalize(loudness); err != nil {
		_, _ = job.im.store.SetFileImportStateAborted(job.Show, job.ID, "normalizing file failed: "+err.Error()) // dear linter: we are not interested in any errors from this function
		return err
	}
	_, _ = job.im.store.SetFileImportStateDone(job.Show, job.ID) // dear linter: we are not interested in any errors from this function
	return err
}

// StartWithTimeout adds job to the work queue of the importer. If timeout is <= 0 the importers
// default job-timeout will be used. Jobs that get started using this function clean up any
// resources when done.
func (job *Job) StartWithTimeout(timeout time.Duration) (err error) {
	if timeout <= 0 {
		timeout = job.im.conf.JobTimeout
	}
	return job.start(context.WithTimeout(context.Background(), timeout))
}

// StartWithContext adds job to the work queue of the importer. You should pass on a context
// that has a deadline or timeout set. Canceling the context will abort the import job. You are
// responsible for cleaning up the context resources (using the CancelFunc) after the job has
// completed.
func (job *Job) StartWithContext(ctx context.Context) (err error) {
	return job.start(context.WithCancel(ctx))
}

func (job *Job) start(ctx context.Context, cancel context.CancelFunc) (err error) {
	if !atomic.CompareAndSwapUint32((*uint32)(&job.State), uint32(JobNew), uint32(JobInitializing)) {
		if atomic.LoadUint32((*uint32)(&job.State)) == uint32(JobCanceled) {
			// the job has already been canceled and thus should just be removed from the inventory
			cancel()
			job.cleanup()
			return ErrAlreadyCanceled
		}
		cancel()
		return
	}

	if job.WorkDir, err = os.MkdirTemp(job.im.conf.TempPath, "tank-job-"); err != nil {
		cancel()
		job.cleanup()
		return
	}

	job.Ctx = ctx
	job.cancel = cancel
	// from here on we need to take care that job.cancel() will be called in any case so that resources
	// allocated by job.Ctx are freed up.

	// although the job is not really enqueued yet we need to update the file state here because after the
	// select there is a race between this update call and the one performed in the beginning of job.run()
	_, _ = job.im.store.SetFileImportStatePending(job.Show, job.ID) // dear linter: we are not interested in any errors from this function
	select {
	case job.im.work <- job:
		if !atomic.CompareAndSwapUint32((*uint32)(&job.State), uint32(JobInitializing), uint32(JobPending)) {
			// we just posted the job onto the work channel but job.Cancel() has apparently been called while
			// we were setting up the context. Because of this job.Cancel() couldn't use the cancel function
			// provided by the context. There is not much that we can do here... job.run() needs to clean up
			// the rest and remove the job from the inventory.
			return ErrAlreadyCanceled
		}
	default:
		// the work channel is already full so we need to drop all new jobs...
		_, _ = job.im.store.SetFileImportStateAborted(job.Show, job.ID, "enqueuing import failed: too many pending jobs") // dear linter: we are not interested in any errors from this function
		job.cancel()
		job.cleanup()
		return ErrTooManyJobs
	}

	// the job was successfully enqueued now and awaits a worker to call job.run().
	return
}

func (job *Job) AttachUploader(len uint64, name string, r io.Reader) (<-chan *JobSourceResult, error) {
	if state := atomic.LoadUint32((*uint32)(&job.State)); state != uint32(JobRunning) {
		return nil, ErrImportNotRunning
	}

	// only allow to attach external sources if the job's source was in fact an upload URL
	if job.Source.Scheme != SourceSchemeUpload {
		return nil, ErrSourceNotAUpload
	}
	if ok := atomic.CompareAndSwapUint32(&job.sourceSet, 0, 1); !ok {
		return nil, ErrSourceAlreadyAttached
	}
	src := newJobSourceUpload(len, name, r)
	job.source = src
	close(job.subC.sourceAttached)
	return src.done, nil
}

func (job *Job) GetAttachedUploader() (io.Reader, error) {
	if atomic.LoadUint32(&job.sourceSet) != 1 {
		return nil, ErrSourceNotYetAttached
	}
	// make sure that AttachUploader() has finished
	<-job.subC.sourceAttached

	src, ok := job.source.(*JobSourceUpload)
	if !ok {
		return nil, ErrSourceNotAUpload
	}
	return src.r, nil
}

func (job *Job) Cancel() {
	oldState := atomic.SwapUint32((*uint32)(&job.State), uint32(JobCanceled))
	// this next line is why we need to make sure JobCanceled is smaller than all the other states.
	if oldState < uint32(JobPending) {
		// job.cancel() might not yet be initialized, setting the state to canceled is the
		// only thing we can do for now... job.Start() or job.run() will take care of the rest.
		return
	}
	job.cancel()
}

func (job *Job) Running() <-chan struct{} {
	return job.subC.running
}

func (job *Job) Done() <-chan struct{} {
	return job.subC.done
}

func (job *Job) cleanup() {
	atomic.StoreUint32((*uint32)(&job.State), uint32(JobDestroying))

	// we are not really interested in the result of the deleteJob here,
	// the only possible error would be ErrNotFound anyway... no need to wait for that.
	go job.im.deleteJob(job.Show, job.ID)

	if job.WorkDir != "" {
		if err := os.RemoveAll(job.WorkDir); err != nil {
			job.im.errLog.Printf("failed to remove jobs work directory: %v", err)
		}
	}
}

func newJob(im *Importer, show string, id uint64, src *SourceURL, user, refID string) *Job {
	job := &Job{im: im, Show: show, ID: id, Source: *src, User: user, RefID: refID}
	job.State = JobNew
	job.CreatedAt.set(time.Now())
	job.subC.sourceAttached = make(chan struct{})
	job.subC.running = make(chan struct{})
	job.subC.done = make(chan struct{})
	return job
}
