//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package importer

import (
	"bufio"
	"io"

	"gitlab.servus.at/autoradio/tank/store"
)

type ffmpegLoudnormParams struct {
	InputI      string `json:"input_i"`
	InputTP     string `json:"input_tp"`
	InputLRA    string `json:"input_lra"`
	InputThresh string `json:"input_thresh"`

	OutputI      string `json:"output_i"`
	OutputTP     string `json:"output_tp"`
	OutputLRA    string `json:"output_lra"`
	OutputThresh string `json:"output_thresh"`

	NormalizationType string `json:"normalization_type"`
	TargetOffset      string `json:"target_offset"`
}

type progressWriter struct {
	job     *Job
	step    JobProgressStep
	total   uint64
	written uint64
	w       io.Writer
}

func (pw *progressWriter) Write(p []byte) (n int, err error) {
	n, err = pw.w.Write(p)
	if n > 0 {
		pw.written += uint64(n)
	}
	pw.job.Progress.set(pw.step, float32(pw.written)/float32(pw.total))
	return
}

type convLogger struct {
	log    *store.Log
	stream string
	s      *bufio.Scanner
	done   chan struct{}
}

func newConvLogger(log *store.Log, stream string, pipe io.Reader) *convLogger {
	l := &convLogger{log: log, stream: stream}
	l.s = bufio.NewScanner(pipe)
	l.done = make(chan struct{})
	go l.run()
	return l
}

func (l *convLogger) run() {
	defer close(l.done)

	for l.s.Scan() {
		l.log.Append(l.stream, l.s.Text())
	}
	if err := l.s.Err(); err != nil {
		l.log.Append(l.stream, l.s.Text())
		return
	}
}

func (l *convLogger) Wait() {
	<-l.done
}
