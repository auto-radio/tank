//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package importer

import (
	"encoding/json"
	"errors"
	"io"
	"math"
	"net/url"
	"strings"
	"sync/atomic"
	"time"

	"gitlab.servus.at/autoradio/tank/store"
)

const (
	DefaultBacklog = 100

	SourceSchemeHTTP   = "http"
	SourceSchemeHTTPs  = "https"
	SourceSchemeFake   = "fake"
	SourceSchemeFile   = "file"
	SourceSchemeUpload = "upload"
)

//******* Errors

var (
	ErrNotImplemented        = errors.New("not implemented")
	ErrNotFound              = errors.New("not found")
	ErrSourceNotSupported    = errors.New("source uri format is not supported")
	ErrImportNotRunning      = errors.New("import not running")
	ErrSourceAlreadyAttached = errors.New("source is already attached")
	ErrSourceNotYetAttached  = errors.New("source is not yet attached")
	ErrSourceNotAUpload      = errors.New("source is not a upload")
	ErrTooManyJobs           = errors.New("too many pending jobs")
	ErrAlreadyCanceled       = errors.New("job is already canceled")
)

//******* Worker: State

type WorkerState uint32

const (
	WorkerNew WorkerState = iota
	WorkerIdle
	WorkerBusy
	WorkerStopped
)

func (s *WorkerState) set(state WorkerState) {
	atomic.StoreUint32((*uint32)(s), uint32(state))
}

func (s *WorkerState) get() (state WorkerState) {
	return WorkerState(atomic.LoadUint32((*uint32)(s)))
}

func (s *WorkerState) String() string {
	switch s.get() {
	case WorkerNew:
		return "new"
	case WorkerIdle:
		return "idle"
	case WorkerBusy:
		return "busy"
	case WorkerStopped:
		return "stopped"
	}
	return "unknown"
}

func (s *WorkerState) MarshalText() (data []byte, err error) {
	data = []byte(s.String())
	return
}

//******* Job: Source URL

type SourceURL url.URL

func (s *SourceURL) String() string {
	return (*url.URL)(s).String()
}

func (s *SourceURL) MarshalText() (data []byte, err error) {
	data = []byte(s.String())
	return
}

func (s *SourceURL) UnmarshalText(data []byte) (err error) {
	_, err = (*url.URL)(s).Parse(string(data))
	return
}

//******* Job: Source

type JobSourceResult struct {
	Err  error      `json:"error,omitempty"`
	Hash string     `json:"hash,omitempty"`
	Log  *store.Log `json:"log"`
}

func (r *JobSourceResult) Error() string {
	return r.Err.Error()
}

type JobSource interface {
	Len() uint64
	String() string
	io.Reader
	Done(*JobSourceResult)
}

//******* Job: State

type JobState uint32

const (
	// it is important that JobCanceled is smaller than any other state of the job
	// see job.Cancel() for why this is!
	JobCanceled JobState = iota
	JobNew
	JobInitializing
	JobPending
	JobRunning
	JobDestroying
)

func (s *JobState) String() string {
	switch JobState(atomic.LoadUint32((*uint32)(s))) {
	case JobNew:
		return "new"
	case JobInitializing:
		return "initializing"
	case JobPending:
		return "pending"
	case JobRunning:
		return "running"
	case JobDestroying:
		return "destroying"
	}
	return "unknown"
}

func (s *JobState) fromString(str string) error {
	switch strings.ToLower(str) {
	case "new":
		atomic.StoreUint32((*uint32)(s), uint32(JobNew))
	case "initializing":
		atomic.StoreUint32((*uint32)(s), uint32(JobInitializing))
	case "pending":
		atomic.StoreUint32((*uint32)(s), uint32(JobPending))
	case "running":
		atomic.StoreUint32((*uint32)(s), uint32(JobRunning))
	case "destroying":
		atomic.StoreUint32((*uint32)(s), uint32(JobDestroying))
	default:
		return errors.New("invalid job state: '" + str + "'")
	}
	return nil
}

func (s *JobState) MarshalText() (data []byte, err error) {
	data = []byte(s.String())
	return
}

func (s *JobState) UnmarshalText(data []byte) (err error) {
	return s.fromString(string(data))
}

//******* Job: Progress

type JobProgressStep uint32

const (
	StepFetching JobProgressStep = iota
	StepNormalizing
)

func (s JobProgressStep) String() string {
	switch s {
	case StepFetching:
		return "fetching"
	case StepNormalizing:
		return "normalizing"
	}
	return "unknown"
}

func (s JobProgressStep) MarshalText() (data []byte, err error) {
	data = []byte(s.String())
	return
}

type JobProgress uint64

func (p *JobProgress) set(step JobProgressStep, progress float32) {
	tmp := uint64(step) << 32
	tmp |= uint64(math.Float32bits(progress))
	atomic.StoreUint64((*uint64)(p), tmp)
}

func (p *JobProgress) get() (step JobProgressStep, progress float32) {
	tmp := atomic.LoadUint64((*uint64)(p))
	step = JobProgressStep(tmp >> 32)
	progress = math.Float32frombits(uint32(tmp))
	return
}

func (p *JobProgress) MarshalJSON() ([]byte, error) {
	step, progress := p.get()
	if math.IsNaN(float64(progress)) || math.IsInf(float64(progress), 0) || progress < 0.0 {
		progress = -1.0
	} else if progress > 1.0 {
		progress = 1.0
	}

	return json.Marshal(struct {
		Step     JobProgressStep `json:"step"`
		Progress float32         `json:"progress"`
	}{step, progress})
}

//******* Timestamp

type Timestamp int64

func (t *Timestamp) set(ts time.Time) {
	atomic.StoreInt64((*int64)(t), ts.Unix())
}

func (t *Timestamp) get() (ts time.Time) {
	return time.Unix(atomic.LoadInt64((*int64)(t)), 0)
}

func (t *Timestamp) MarshalJSON() (data []byte, err error) {
	return t.get().MarshalJSON()
}
