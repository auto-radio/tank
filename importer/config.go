//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package importer

import (
	"errors"
	"os"
	"strings"
	"time"
)

type NormalizerType int

const (
	NormNone NormalizerType = iota
	NormFFmpeg
)

func (c *NormalizerType) String() string {
	switch *c {
	case NormNone:
		return "none"
	case NormFFmpeg:
		return "ffmpeg"
	}
	return "unknown"
}

func (c *NormalizerType) fromString(str string) error {
	switch strings.ToLower(os.ExpandEnv(str)) {
	case "none":
		*c = NormNone
	case "ffmpeg":
		*c = NormFFmpeg
	default:
		return errors.New("invalid normalizer: '" + str + "'")
	}
	return nil
}

func (c *NormalizerType) MarshalText() (data []byte, err error) {
	data = []byte(c.String())
	return
}

func (c *NormalizerType) UnmarshalText(data []byte) (err error) {
	return c.fromString(string(data))
}

type Config struct {
	JobTimeout time.Duration  `json:"job-timeout" yaml:"job-timeout" toml:"job-timeout"`
	TempPath   string         `json:"temp-path" yaml:"temp-path" toml:"temp-path"`
	Workers    int            `json:"workers" yaml:"workers" toml:"workers"`
	Backlog    uint           `json:"backlog" yaml:"backlog" toml:"backlog"`
	Normalizer NormalizerType `json:"normalizer" yaml:"normalizer" toml:"normalizer"`
}

func (c *Config) ExpandEnv() {
	c.TempPath = os.ExpandEnv(c.TempPath)
}
