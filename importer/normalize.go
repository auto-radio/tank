//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package importer

import (
	"io"
	"os"
	"path/filepath"
)

func (job *Job) copyToNormalizer(w io.Writer, done chan<- error) {
	defer close(done)

	src, err := os.OpenFile(filepath.Join(job.WorkDir, "source"), os.O_RDONLY, 0400)
	if err != nil {
		done <- err
		return
	}
	srcStat, err := src.Stat()
	if err != nil {
		done <- err
		return
	}
	if deadline, ok := job.Ctx.Deadline(); ok {
		_ = src.SetDeadline(deadline) // this will probably fail but we try it anyway
	}

	written, err := io.Copy(&progressWriter{job, StepNormalizing, uint64(srcStat.Size()), 0, w}, src)
	if err != nil {
		done <- err
		return
	}
	job.im.dbgLog.Printf("normalize(): done copying %d bytes from source", written)
}

func (job *Job) normalize(loudness interface{}) error {
	job.Progress.set(StepNormalizing, 0)

	conv, err := job.newNormalizeConverter(loudness)
	if err != nil {
		job.im.errLog.Printf("normalize(): creating normalize converter failed: %v", err)
		return err
	}
	// from here on conv.Close() and conv.Wait() has to be called in any case to
	// reap potential child process zombies

	done := make(chan error)
	go job.copyToNormalizer(conv, done)

	select {
	case <-job.Ctx.Done():
		conv.Close()
		go conv.Wait() // do the zombie reaping in seperate go routine since we are not interested in the result anyway
		return job.Ctx.Err()
	case err = <-done:
	}

	conv.Close()
	convLog, convErr := conv.Wait()
	job.im.dbgLog.Printf("normalize(): converter returned: %v", err)
	if err == nil {
		err = convErr
	}

	if err := job.im.store.AddImportLog(job.Show, job.ID, "normalize", convLog); err != nil {
		job.im.infoLog.Printf("fetch(): failed to add normalize log output to store: %v", err)
	}
	// TODO: also write convLog to dbgLog?

	job.Progress.set(StepNormalizing, 1)
	return err
}
