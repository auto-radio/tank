package main

import (
	"fmt"
	"net"
	"os"
	"strconv"
	"time"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Println("Usage: go run waitfor-tcp.go <host:port> <retries>")
		os.Exit(1)
	}
	hostPort := os.Args[1]
	retries, err := strconv.Atoi(os.Args[2])
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	if retries < 0 {
		fmt.Println("retry value must be positive")
		os.Exit(1)
	}

	fmt.Printf("Trying to connect to %s ", hostPort)
	n := 0
	for {
		if _, err := net.DialTimeout("tcp", hostPort, 100*time.Millisecond); err == nil {
			fmt.Println(" ok")
			os.Exit(0)
		}
		fmt.Print(".")
		n = n + 1
		if n > retries {
			fmt.Println(" fail")
			os.Exit(2)
		}
		time.Sleep(1 * time.Second)
	}
}
