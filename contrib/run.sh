#!/bin/bash

BASE_D=$(realpath "${BASH_SOURCE%/*}/..")
CONFIG="$BASE_D/tank.yaml"

export AURA_TANK_DEBUG=1
export AURA_TANK_LISTEN=127.0.0.1:8040
export STORE_PATH="/run/user/${UID}/aura-tank"
mkdir -p "${STORE_PATH}"

export ADMIN_PASSWORD="very-secret"
export AURA_ENGINE_SECRET="rather-secret"

export OIDC_CLIENT_ID="937574"
export OIDC_CLIENT_SECRET="ovvJzW2H98bZe2TGxvdhHjLs15xEcr9y"

[ -f $CONFIG ] && "$BASE_D/tank" --config "$CONFIG" run || echo "tank.yaml is missing"
