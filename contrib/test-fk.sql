\c tank

/* should succeed */
insert into shows (name) values ('test');

/* should fail */
insert into files (size) values(100);
insert into files (show_name, size) values('invalid', 101);

/* should succeed */
insert into files (show_name, size) values('test', 101);
insert into files (show_name, size) values('test', 102);
select * from files;


/* should fail */
insert into playlists (created_at) values ('2018-06-17 02:38:17');

/* should succeed */
insert into playlists (created_at, show_name) values('2018-06-17 02:38:17', 'test');


/* should fail */
insert into playlist_entries (uri) values ('http://stream.example.com/live.mp3');
insert into playlist_entries (playlist_id, uri) values (2, 'http://stream.example.com/live.mp3');
insert into playlist_entries (playlist_id, line_num, uri) values (17, 1, 'http://stream.example.com/live.mp3');

/* should succeed */
insert into playlist_entries (playlist_id, line_num, uri) values (2, 1, 'http://stream.example.com/live.mp3');

/* should fail */
insert into playlist_entries (playlist_id, line_num, uri) values (2, 1, 'http://stream.example.com/other.mp3');
insert into playlist_entries (playlist_id, line_num, file_id) values (2, 4, 23);

/* should succeed */
insert into playlist_entries (playlist_id, line_num, file_id) values (2, 4, 4);
delete from files where id = 3;

/* should fail */
delete from files where id = 4;

/* should succeed */
delete from playlists where id = 2;
delete from files where id = 4;
