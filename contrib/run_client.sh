#!/bin/bash

exec docker exec -it tank-postgres /bin/bash -c "echo \"alias psql='psql -U tank'\" > /tmp/.bashrc-psql; cd /scripts/; exec bash --rcfile /tmp/.bashrc-psql"
