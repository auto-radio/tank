#!/bin/bash

SCRIPTS_D=$(realpath "${BASH_SOURCE%/*}/")
exec docker run -it --rm --name tank-postgres -e POSTGRES_USER=tank -e POSTGRES_PASSWORD=aura -v "$SCRIPTS_D:/scripts:ro" -p 5432:5432 postgres:14
