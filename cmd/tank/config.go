//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package main

import (
	"errors"
	"os"
	"time"

	"gitlab.servus.at/autoradio/tank/auth"
	"gitlab.servus.at/autoradio/tank/importer"
	"gitlab.servus.at/autoradio/tank/store"
	"gopkg.in/yaml.v3"
)

type CorsConfig struct {
	AllowedOrigins     []string `json:"allowed-origins" yaml:"allowed-origins" toml:"allowed-origins"`
	AllowedMethods     []string `json:"allowed-methods" yaml:"allowed-methods" toml:"allowed-methods"`
	AllowedHeaders     []string `json:"allowed-headers" yaml:"allowed-headers" toml:"allowed-headers"`
	ExposedHeaders     []string `json:"exposed-headers" yaml:"exposed-headers" toml:"exposed-headers"`
	MaxAge             int      `json:"max-age" yaml:"max-age" toml:"max-age"`
	AllowCredentials   bool     `json:"allow-credentials" yaml:"allow-credentials" toml:"allow-credentials"`
	OptionsPassthrough bool     `json:"options-passthrough" yaml:"options-passthrough" toml:"options-passthrough"`
	Debug              bool     `json:"debug" yaml:"debug" toml:"debug"`
}

type WebConfig struct {
	AccessLogs   string        `json:"access-logs" yaml:"access-logs" toml:"access-logs"`
	ReadTimeout  time.Duration `json:"read-timeout" yaml:"read-timeout" toml:"read-timeout"`
	WriteTimeout time.Duration `json:"write-timeout" yaml:"write-timeout" toml:"write-timeout"`
	DebugUI      struct {
		Enabled bool `json:"enabled" yaml:"enabled" toml:"enabled"`
	} `json:"debug-ui" yaml:"debug-ui" toml:"debug-ui"`
	Cors *CorsConfig `json:"cors" yaml:"cors" toml:"cors"`
}

type Config struct {
	Store    store.Config    `json:"store" yaml:"store" toml:"store"`
	Importer importer.Config `json:"importer" yaml:"importer" toml:"importer"`
	Auth     *auth.Config    `json:"auth" yaml:"auth" toml:"auth"`
	Web      WebConfig       `json:"web" yaml:"web" toml:"web"`
}

func (c *Config) ExpandEnv() {
	c.Store.ExpandEnv()
	c.Importer.ExpandEnv()
	if c.Auth != nil {
		c.Auth.ExpandEnv()
	}
	c.Web.AccessLogs = os.ExpandEnv(c.Web.AccessLogs)
	if c.Web.Cors != nil {
		// TODO: implement this...
	}
}

func ReadConfig(configfile string) (conf *Config, err error) {
	var f *os.File
	if f, err = os.Open(configfile); err != nil {
		err = errors.New("error opening config file(" + configfile + "): " + err.Error())
		return
	}
	defer f.Close()

	conf = &Config{}
	decoder := yaml.NewDecoder(f)
	decoder.KnownFields(true)
	if err = decoder.Decode(conf); err != nil {
		err = errors.New("error opening config file(" + configfile + "): " + err.Error())
		return
	}
	conf.ExpandEnv()
	infoLog.Printf("successfully read config file (%s)", configfile)
	return
}
