//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package main

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"
	swaggerFiles "github.com/swaggo/files"
	"github.com/swaggo/gin-swagger"
	_ "gitlab.servus.at/autoradio/tank/api/docs"
	apiV1 "gitlab.servus.at/autoradio/tank/api/v1"
	"gitlab.servus.at/autoradio/tank/auth"
	"gitlab.servus.at/autoradio/tank/importer"
	"gitlab.servus.at/autoradio/tank/store"
	"gitlab.servus.at/autoradio/tank/ui"
)

const (
	WebUIPathPrefix  = "/ui/"
	WebAuthPrefix    = "/auth/"
	WebAPIDocsPrefix = "/api/docs/"
	WebAPIv1Prefix   = "/api/v1/"
	HealthzEndpoint  = "/healthz"
)

func apache2CombinedLogger(param gin.LogFormatterParams) string {
	return fmt.Sprintf("%s - - [%s] \"%s %s %s\" %d %d \"-\" \"%s\"\n",
		param.ClientIP,
		param.TimeStamp.Format(time.RFC1123),
		param.Method,
		param.Path,
		param.Request.Proto,
		param.StatusCode,
		param.BodySize,
		param.Request.UserAgent(),
	)
}

func installLogger(r *gin.Engine, conf WebConfig) error {
	if conf.AccessLogs == "" {
		return nil
	}

	var logConfig gin.LoggerConfig
	target := strings.SplitN(conf.AccessLogs, ":", 2)
	switch strings.ToLower(target[0]) {
	case "stdout":
		logConfig.Output = os.Stdout
	case "stderr":
		logConfig.Output = os.Stderr
	case "file":
		if len(target) != 2 {
			return fmt.Errorf("please specify a path to the access log")
		}
		f, err := os.OpenFile(target[1], os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			return fmt.Errorf("failed to open access log: %v", err)
		}
		logConfig.Output = f
		logConfig.Formatter = apache2CombinedLogger
	default:
		return fmt.Errorf("unknown log target type: %q", target[0])
	}

	r.Use(gin.LoggerWithConfig(logConfig))

	return nil
}

type HealthStatus struct {
	err error
}

func (s HealthStatus) String() string {
	if s.err == nil {
		return "OK"
	}
	return s.err.Error()
}

func (s HealthStatus) MarshalText() (data []byte, err error) {
	data = []byte(s.String())
	return
}

type Health struct {
	Auth     HealthStatus `json:"auth" swaggertype:"string"`
	Store    HealthStatus `json:"store" swaggertype:"string"`
	Importer HealthStatus `json:"importer" swaggertype:"string"`
}

// healthzHandler checks daemon health.
// @Summary      Check health
// @Description  Checks daemon health.
// @Produce      json
// @Success      200  {object}  Health
// @Failure      503  {object}  Health
// @Router       /healthz [get]
func healthzHandler(c *gin.Context, st *store.Store, im *importer.Importer) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second) // TODO: hardcoded value
	defer cancel()

	var h Health
	var wg sync.WaitGroup
	wg.Add(3)
	go func() { defer wg.Done(); h.Auth.err = auth.Healthz(ctx) }()
	go func() { defer wg.Done(); h.Store.err = st.Healthz(ctx) }()
	go func() { defer wg.Done(); h.Importer.err = im.Healthz(ctx) }()

	wg.Wait()
	code := http.StatusOK
	if h.Auth.err != nil || h.Store.err != nil || h.Importer.err != nil {
		code = http.StatusServiceUnavailable
	}
	c.JSON(code, h)
}

func runWeb(ln net.Listener, st *store.Store, im *importer.Importer, conf WebConfig) error {
	gin.SetMode(gin.ReleaseMode)

	r := gin.New()
	r.Use(gin.Recovery())
	if err := installLogger(r, conf); err != nil {
		return err
	}
	r.HandleMethodNotAllowed = true
	if conf.Cors != nil {
		c := cors.New(cors.Options{
			AllowedOrigins:     conf.Cors.AllowedOrigins,
			AllowedMethods:     conf.Cors.AllowedMethods,
			AllowedHeaders:     conf.Cors.AllowedHeaders,
			ExposedHeaders:     conf.Cors.ExposedHeaders,
			MaxAge:             conf.Cors.MaxAge,
			AllowCredentials:   conf.Cors.AllowCredentials,
			OptionsPassthrough: conf.Cors.OptionsPassthrough,
			Debug:              conf.Cors.Debug,
		})
		r.Use(c)
	}

	if conf.DebugUI.Enabled {
		infoLog.Printf("web: enabling debug ui")
		r.GET("/", func(c *gin.Context) { c.Redirect(http.StatusSeeOther, WebUIPathPrefix) })
		r.StaticFS(WebUIPathPrefix, ui.Assets)
	}

	apiV1.InstallHTTPHandler(r.Group(WebAPIv1Prefix), st, im, infoLog, errLog, dbgLog)
	auth.InstallHTTPHandler(r.Group(WebAuthPrefix))
	r.GET(HealthzEndpoint, func(c *gin.Context) { healthzHandler(c, st, im) })
	r.GET(WebAPIDocsPrefix+"*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	srv := &http.Server{
		Handler:      r,
		WriteTimeout: 12 * time.Hour,
		ReadTimeout:  12 * time.Hour,
	}
	if conf.ReadTimeout > 0 {
		srv.ReadTimeout = conf.ReadTimeout
	}
	if conf.WriteTimeout > 0 {
		srv.WriteTimeout = conf.WriteTimeout
	}

	infoLog.Printf("web: listening on %s", ln.Addr())
	return srv.Serve(ln)
}
