//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"path/filepath"

	"github.com/codegangsta/cli"
	"github.com/coreos/go-systemd/activation"
	"gitlab.servus.at/autoradio/tank/auth"
	"gitlab.servus.at/autoradio/tank/importer"
	"gitlab.servus.at/autoradio/tank/store"
)

var (
	infoLog = getInfoLog("[ --- ] ")
	errLog  = getErrLog("[ ERR ] ")
	dbgLog  = getDbgLog("[ DBG ] ")
)

func getInfoLog(prefix string) *log.Logger {
	logsDir := os.Getenv("AURA_LOGS")
	if logsDir == "" {
		logsDir = "/srv/logs"
	}

	file, err := os.OpenFile(filepath.Join(logsDir, "info.log"), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0664)
	if err != nil {
		log.Fatal(err)
	}

	mw := io.MultiWriter(os.Stdout, file)

	log.SetOutput(mw)

	return log.New(mw, prefix, 0)
}

func getErrLog(prefix string) *log.Logger {
	logsDir := os.Getenv("AURA_LOGS")
	if logsDir == "" {
		logsDir = "/srv/logs"
	}

	file, err := os.OpenFile(filepath.Join(logsDir, "error.log"), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0664)
	if err != nil {
		log.Fatal(err)
	}

	log.SetOutput(file)

	return log.New(file, prefix, 0)
}

func getDbgLog(prefix string) *log.Logger {
	dbg := log.New(io.Discard, prefix, 0)
	if _, exists := os.LookupEnv("AURA_TANK_DEBUG"); exists {
		dbg.SetOutput(os.Stdout)
	}
	return dbg
}

func cmdInit(c *cli.Context) error {
	conf, err := ReadConfig(c.GlobalString("config"))
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	st, err := store.NewStore(conf.Store)
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}
	infoLog.Printf("successfully initialized store (database revision: %s)", st.GetRevision())

	return cli.NewExitError("", 0)
}

func openStoreAndImporter(conf *Config) (st *store.Store, im *importer.Importer, err error) {
	if st, err = store.NewStore(conf.Store); err != nil {
		return nil, nil, err
	}
	infoLog.Printf("successfully opend new store (database revision: %s)", st.GetRevision())

	if im, err = importer.NewImporter(conf.Importer, st, infoLog, errLog, dbgLog); err != nil {
		return nil, nil, err
	}

	return
}

func cmdRun(c *cli.Context) error {
	infoLog.Println("starting...")

	conf, err := ReadConfig(c.GlobalString("config"))
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	if err := auth.Init(conf.Auth, infoLog, errLog, dbgLog); err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	st, im, err := openStoreAndImporter(conf)
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	ln, err := net.Listen("tcp", c.String("listen"))
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	if err = runWeb(ln, st, im, conf.Web); err != nil {
		return cli.NewExitError(err.Error(), 1)
	}
	return cli.NewExitError("", 0)
}

func cmdRunSa(c *cli.Context) error {
	infoLog.Println("starting (socket activated) ...")

	conf, err := ReadConfig(c.GlobalString("config"))
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	if err := auth.Init(conf.Auth, infoLog, errLog, dbgLog); err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	st, im, err := openStoreAndImporter(conf)
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	lns, err := activation.Listeners()
	if err != nil {
		return cli.NewExitError(fmt.Sprintf("fetching socket listeners from systemd failed: %s", err), 2)
	}

	infoLog.Printf("got %d sockets from systemd\n", len(lns))
	if len(lns) != 1 {
		errLog.Printf("we need exactly one listener, shutting down.")
		return cli.NewExitError("", 2)
	}

	if err = runWeb(lns[0], st, im, conf.Web); err != nil {
		return cli.NewExitError(err.Error(), 1)
	}
	return cli.NewExitError("", 0)
}

func main() {
	app := cli.NewApp()
	app.Name = "tank"
	app.Version = "0.0.1"
	app.Usage = "import & playlist daemon"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "config",
			Value:  "/etc/aura/tank.yaml",
			Usage:  "path to the configuration file",
			EnvVar: "AURA_TANK_CONFIG",
		},
	}
	app.Commands = []cli.Command{
		{
			Name:   "init",
			Usage:  "initialize the store",
			Action: cmdInit,
		},
		{
			Name:  "run",
			Usage: "run the daemon",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:   "listen",
					Usage:  "address to listen on for web API",
					EnvVar: "AURA_TANK_LISTEN",
				},
			},
			Action: cmdRun,
		},
		{
			Name:   "runsa",
			Usage:  "run the daemon (using systemd socket-activation)",
			Action: cmdRunSa,
		},
	}

	_ = app.Run(os.Args) // dear linter: we are ingoring errors here, we can't do anything usefull with it anyway
}
