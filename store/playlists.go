//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package store

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"

	"github.com/jinzhu/gorm"
)

func generateFileURI(file *File) string {
	uri := url.URL{Scheme: FileURIScheme, Host: file.ShowName, Path: strconv.FormatUint(file.ID, 10)}
	return uri.String()
}

func (p *Playlist) BeforeSave(tx *gorm.DB) error {
	referencedFiles := make(map[uint64]*File)
	hasEntryWithoutDuration := false
	for idx := range p.Entries {
		p.Entries[idx].LineNum = uint(idx)
		if p.Entries[idx].Duration != nil && *(p.Entries[idx].Duration) < 0 {
			return ErrInvalidPlaylistEntry{idx, "negative durations are not allowed"}
		}

		if p.Entries[idx].File != nil {
			if p.Entries[idx].File.ShowName == "" {
				p.Entries[idx].File.ShowName = p.ShowName
			}
			referencedFiles[p.Entries[idx].File.ID] = nil
			continue
		}
		if p.Entries[idx].URI == "" {
			return ErrInvalidPlaylistEntry{idx, "entries must either reference a File or have a URI set"}
		}

		uri, err := url.Parse(p.Entries[idx].URI)
		if err != nil {
			return ErrInvalidPlaylistEntry{idx, err.Error()}
		}
		if uri.Scheme == FileURIScheme {
			if uri.Host == "" || uri.Path == "" {
				return ErrInvalidPlaylistEntry{idx, "uri must be in the format " + FileURIScheme + "://<show>/<file-id>"}
			}
			fileID, err := strconv.ParseUint(strings.TrimPrefix(uri.Path, "/"), 10, 64)
			if err != nil {
				return ErrInvalidPlaylistEntry{idx, err.Error()}
			}
			p.Entries[idx].File = &File{ID: fileID, ShowName: uri.Host}
			referencedFiles[fileID] = nil
			p.Entries[idx].URI = "" // this will be regenerated in AfterFind()
		} else if p.Entries[idx].Duration == nil {
			if hasEntryWithoutDuration {
				return ErrPlaylistHasMultipleNullDurationEntries
			}
			hasEntryWithoutDuration = true
		}
	}

	var fileEntryIDs []uint64
	for id, _ := range referencedFiles {
		fileEntryIDs = append(fileEntryIDs, id)
	}
	var files []*File
	if err := tx.Where("id IN (?)", fileEntryIDs).Model(&File{}).Select("id, show_name, duration").Scan(&files).Error; err != nil {
		return nil
	}
	for _, file := range files {
		referencedFiles[file.ID] = file
	}

	for idx := range p.Entries {
		if p.Entries[idx].File == nil {
			continue
		}
		if referencedFiles[p.Entries[idx].File.ID] == nil || referencedFiles[p.Entries[idx].File.ID].ShowName != p.Entries[idx].File.ShowName {
			return ErrInvalidPlaylistEntry{idx, fmt.Sprintf("file '%s/%d' does not exist", p.Entries[idx].File.ShowName, p.Entries[idx].File.ID)}
		}
		if p.Entries[idx].Duration != nil && referencedFiles[p.Entries[idx].File.ID].Duration != *(p.Entries[idx].Duration) {
			return ErrInvalidPlaylistEntry{idx, "provided duration and file duration mismatch"}
		}
		p.Entries[idx].Duration = nil // this will be regenerated in AfterFind()
	}

	return nil
}

func (p *Playlist) AfterSave(tx *gorm.DB) error {
	return tx.Preload("Entries.File").First(p, p.ID).Error
}

func (p *Playlist) AfterFind() error {
	for idx := range p.Entries {
		if p.Entries[idx].File != nil {
			p.Entries[idx].URI = generateFileURI(p.Entries[idx].File)
			p.Entries[idx].Duration = &p.Entries[idx].File.Duration
		}
	}
	return nil
}

func (st *Store) ListPlaylists(show string, offset, limit int) (playlists []Playlist, err error) {
	err = st.db.Where("show_name = ?", show).Preload("Entries", func(db *gorm.DB) *gorm.DB {
		return db.Order("playlist_entries.line_num asc")
	}).Preload("Entries.File").Order("id").Offset(offset).Limit(limit).Find(&playlists).Error
	return
}

func (st *Store) CreatePlaylist(show string, playlist Playlist) (*Playlist, error) {
	if _, err := st.CreateShow(show); err != nil {
		return nil, err
	}
	playlist.ID = 0
	playlist.ShowName = show
	err := st.db.Create(&playlist).Error
	return &playlist, err
}

func (st *Store) GetPlaylist(show string, id uint64) (playlist *Playlist, err error) {
	playlist = &Playlist{}
	// we have to make sure that the playlist actually belongs to <show> since permissions are enforced
	// based on show membership
	err = st.db.Where("show_name = ?", show).Preload("Entries", func(db *gorm.DB) *gorm.DB {
		return db.Order("playlist_entries.line_num ASC")
	}).Preload("Entries.File").First(playlist, id).Error
	return
}

func (st *Store) UpdatePlaylist(show string, id uint64, playlist Playlist) (out *Playlist, err error) {
	tx := st.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			if err == nil {
				err = fmt.Errorf("runtime panic: %+v", r)
			}
		}
	}()
	if err = tx.Error; err != nil {
		return
	}

	// make sure the file exists and actually belongs to <show> since permissions are enforced
	// based on show membership
	if err = tx.Where("show_name = ?", show).First(&Playlist{}, id).Error; err != nil {
		tx.Rollback()
		return
	}

	if err = tx.Delete(&PlaylistEntry{}, "playlist_id = ?", id).Error; err != nil {
		tx.Rollback()
		return
	}

	playlist.ID = id
	playlist.ShowName = show
	if err = tx.Save(&playlist).Error; err != nil {
		tx.Rollback()
		return
	}
	err = tx.Commit().Error
	out = &playlist
	return
}

func (st *Store) DeletePlaylist(show string, id uint64) (err error) {
	// we have to make sure that the playlist actually belongs to <show> since permissions are enforced
	// based on show membership
	result := st.db.Where("show_name = ?", show).Delete(&Playlist{ID: id})
	if err = result.Error; err != nil {
		return
	}
	if result.RowsAffected == 0 {
		return ErrNotFound
	}
	return
}

func (st *Store) ListPlaylistsAllShows(offset, limit int) (playlists []Playlist, err error) {
	err = st.db.Preload("Entries", func(db *gorm.DB) *gorm.DB {
		return db.Order("playlist_entries.line_num asc")
	}).Preload("Entries.File").Order("id").Offset(offset).Limit(limit).Find(&playlists).Error
	return
}

func (st *Store) GetPlaylistAllShows(id uint64) (playlist *Playlist, err error) {
	playlist = &Playlist{}
	err = st.db.Preload("Entries", func(db *gorm.DB) *gorm.DB {
		return db.Order("playlist_entries.line_num ASC")
	}).Preload("Entries.File").First(playlist, id).Error
	return
}
