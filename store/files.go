//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package store

import (
	"fmt"
	"os"
	"path/filepath"
)

func (st *Store) GetFilePath(show string, id uint64) string {
	filename := fmt.Sprintf("%d%s", id, st.Audio.Format.Extension())
	return filepath.Join(st.getShowPath(show), filename)
}

func (st *Store) ListFiles(show string, offset, limit int) (files []File, err error) {
	err = st.db.Where("show_name = ?", show).Order("id").Offset(offset).Limit(limit).Find(&files).Error
	return
}

func (st *Store) CreateFile(show string, file File) (*File, error) {
	if _, err := st.CreateShow(show); err != nil {
		return nil, err
	}
	file.ID = 0
	file.ShowName = show
	err := st.db.Create(&file).Error
	return &file, err
}

func (st *Store) GetFile(show string, id uint64) (file *File, err error) {
	file = &File{}
	// we have to make sure that the file actually belongs to <show> since permissions are enforced
	// based on show membership
	err = st.db.Where("show_name = ?", show).First(file, id).Error
	return
}

func (st *Store) UpdateFile(show string, id uint64, file File) (out *File, err error) {
	tx := st.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			if err == nil {
				err = fmt.Errorf("runtime panic: %+v", r)
			}
		}
	}()
	if err = tx.Error; err != nil {
		return
	}

	// make sure the file exists and actually belongs to <show> since permissions are enforced
	// based on show membership
	if err = tx.Where("show_name = ?", show).First(&File{}, id).Error; err != nil {
		tx.Rollback()
		return
	}

	file.ID = id
	file.ShowName = show
	if err = tx.Save(&file).Error; err != nil {
		tx.Rollback()
		return
	}
	err = tx.Commit().Error
	out = &file
	return
}

func (st *Store) UpdateFileMetadata(show string, id uint64, metadata map[string]string) (file *File, err error) {
	tx := st.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			if err == nil {
				err = fmt.Errorf("runtime panic: %+v", r)
			}
		}
	}()
	if err = tx.Error; err != nil {
		return
	}

	file = &File{ID: id}
	// make sure the file exists and actually belongs to <show> since permissions are enforced
	// based on show membership
	if err = tx.Where("show_name = ?", show).First(&file).Error; err != nil {
		tx.Rollback()
		return
	}
	if file.Source.Import.State != ImportDone {
		tx.Rollback()
		err = ErrFileImportNotDone
		return
	}

	var fields map[string]interface{}
	if fields, err = st.metadataFieldsToFile(show, id, metadata); err != nil {
		tx.Rollback()
		return
	}
	if err = tx.Model(&file).Where("show_name = ?", show).Update(fields).Error; err != nil {
		tx.Rollback()
		return
	}
	err = tx.Commit().Error
	return
}

func (st *Store) updateFile(show string, id uint64, values ...interface{}) (file *File, err error) {
	file = &File{ID: id}
	if err = st.db.Model(&file).Where("show_name = ?", show).Update(values...).Error; err != nil {
		return nil, err
	}
	return file, nil
}

func (st *Store) SetFileImportStateInitializing(show string, id uint64) (file *File, err error) {
	file = &File{ID: id}
	result := st.db.Model(&file).Where("show_name = ? and source__import__state = ?", show, ImportNew).Update("source__import__state", ImportInitializing)
	if result.Error != nil {
		return nil, result.Error
	}
	if result.RowsAffected != 1 {
		return nil, ErrFileNotNew
	}

	return file, nil
}

func (st *Store) SetFileImportStatePending(show string, id uint64) (file *File, err error) {
	return st.updateFile(show, id, "source__import__state", ImportPending)
}

func (st *Store) SetFileImportStateRunning(show string, id uint64) (file *File, err error) {
	return st.updateFile(show, id, "source__import__state", ImportRunning)
}

func (st *Store) SetFileImportStateDone(show string, id uint64) (*File, error) {
	fields, err := st.metadataFieldsFromFile(show, id)
	if err != nil {
		return nil, err
	}
	fields["source__import__state"] = ImportDone

	return st.updateFile(show, id, fields)
}

func (st *Store) SetFileImportStateAborted(show string, id uint64, error string) (file *File, err error) {
	fields := make(map[string]interface{})
	fields["source__import__state"] = ImportAborted
	fields["source__import__error"] = error

	return st.updateFile(show, id, fields)
}

func (st *Store) UpdateFileSourceHash(show string, id uint64, hash string) (*File, error) {
	return st.updateFile(show, id, "source__hash", hash)
}

func (st *Store) getFileUsage(id uint64, playlists *[]Playlist) (err error) {
	sub := st.db.Model(PlaylistEntry{}).Select("playlist_id").Where("file_id = ?", id).Group("playlist_id").SubQuery()
	err = st.db.Where("id in ?", sub).Find(playlists).Error
	return
}

func (st *Store) GetFileUsage(show string, id uint64) (playlists []Playlist, err error) {
	// make sure the file exists and actually belongs to <show> since permissions are enforced
	// based on show membership
	cnt := 0
	if err = st.db.Model(&File{ID: id}).Where("show_name = ?", show).Count(&cnt).Error; err != nil {
		return
	}
	if cnt == 0 {
		return nil, ErrNotFound
	}
	err = st.getFileUsage(id, &playlists)
	return
}

func (st *Store) DeleteFile(show string, id uint64) (err error) {
	// make sure the file actually belongs to <show> since permissions are enforced
	// based on show membership
	result := st.db.Where("show_name = ?", show).Delete(&File{ID: id})
	if err = result.Error; err != nil {
		// we assume this is due to a FK constraint -> file in use by playlist_entry
		usageErr := &ErrFileInUse{}
		if err = st.getFileUsage(id, &usageErr.Playlists); err != nil {
			return
		}
		return usageErr
	}
	if result.RowsAffected == 0 {
		return ErrNotFound
	}

	filename := st.GetFilePath(show, id)
	if err := os.Remove(filename); err != nil && !os.IsNotExist(err) {
		return fmt.Errorf("unable to delete file '%s': %v", filename, err)
	}
	return
}
