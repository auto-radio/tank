//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package store

import (
	"errors"
	"os"
	"strings"
)

type AudioFormat int

const (
	FormatWAV AudioFormat = iota
	FormatFlac
)

func (f AudioFormat) String() string {
	switch f {
	case FormatWAV:
		return "wav"
	case FormatFlac:
		return "flac"
	}
	return "unknown"
}

func (f *AudioFormat) fromString(str string) error {
	switch strings.ToLower(os.ExpandEnv(str)) {
	case "wav":
		*f = FormatWAV
	case "flac":
		*f = FormatFlac
	default:
		return errors.New("invalid audio format: '" + str + "'")
	}
	return nil
}

func (f AudioFormat) MarshalText() (data []byte, err error) {
	data = []byte(f.String())
	return
}

func (f *AudioFormat) UnmarshalText(data []byte) (err error) {
	return f.fromString(string(data))
}

func (f AudioFormat) Extension() string {
	switch f {
	case FormatWAV:
		return ".wav"
	case FormatFlac:
		return ".flac"
	}
	return ""
}

type AudioConfig struct {
	Format     AudioFormat `json:"format" yaml:"format" toml:"format"`
	SampleRate uint        `json:"sample-rate" yaml:"sample-rate" toml:"sample-rate"`
}

type DBConfig struct {
	Type     string `json:"type" yaml:"type" toml:"type"`
	Host     string `json:"host" yaml:"host" toml:"host"`
	Port     uint16 `json:"port" yaml:"port" toml:"port"`
	TLS      string `json:"tls" yaml:"tls" toml:"tls"`
	Username string `json:"username" yaml:"username" toml:"username"`
	Password string `json:"password" yaml:"password" toml:"password"`
	DB       string `json:"database" yaml:"database" toml:"database"`
	Charset  string `json:"charset" yaml:"charset" toml:"charset"`
	Debug    bool   `json:"debug" yaml:"debug" toml:"debug"`
}

type Config struct {
	BasePath string      `json:"path" yaml:"path" toml:"path"`
	Audio    AudioConfig `json:"audio" yaml:"audio" toml:"audio"`
	DB       DBConfig    `json:"db" yaml:"db" toml:"db"`
}

func (c *Config) ExpandEnv() {
	c.BasePath = os.ExpandEnv(c.BasePath)
	c.DB.Type = os.ExpandEnv(c.DB.Type)
	c.DB.Host = os.ExpandEnv(c.DB.Host)
	c.DB.TLS = os.ExpandEnv(c.DB.TLS)
	c.DB.Username = os.ExpandEnv(c.DB.Username)
	c.DB.Password = os.ExpandEnv(c.DB.Password)
	c.DB.DB = os.ExpandEnv(c.DB.DB)
	c.DB.Charset = os.ExpandEnv(c.DB.Charset)
}
