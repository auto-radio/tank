//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package store

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
	"time"
)

type ffprobeFormat struct {
	NumStreams  uint              `json:"nb_streams"`
	NumPrograms uint              `json:"nb_programs"`
	FormatName  string            `json:"format_name"`
	DurationSec string            `json:"duration"`
	Size        string            `json:"size"`
	BitRate     string            `json:"bit_rate"`
	Tags        map[string]string `json:"tags"`
}

type ffprobeOutput struct {
	Format ffprobeFormat `json:"format"`
}

func (st *Store) readMetadataFromFile(filename string) (duration time.Duration, size uint64, tags map[string]string, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()
	// cmd := exec.CommandContext(ctx, "ffprobe", "-hide_banner", "-print_format", "json", "-show_format", filename)
	cmd := exec.CommandContext(ctx, "ffprobe", "-print_format", "json", "-show_format", filename)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	if err = cmd.Run(); err != nil {
		if exiterr, ok := err.(*exec.ExitError); ok {
			if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
				// TODO: handle stderr?
				err = fmt.Errorf("ffprobe returned %d", status.ExitStatus())
				return
			}
		}
		err = fmt.Errorf("getting exit code of ffprobe failed: %v", err)
		return
	}

	var output ffprobeOutput
	if err = json.NewDecoder(&stdout).Decode(&output); err != nil {
		return
	}
	if duration, err = time.ParseDuration(output.Format.DurationSec + "s"); err != nil {
		err = fmt.Errorf("unable to parse duration from ffprobe output: %v", err)
		return
	}
	if size, err = strconv.ParseUint(output.Format.Size, 10, 64); err != nil {
		err = fmt.Errorf("unable to parse file-size from ffprobe output: %v", err)
		return
	}
	tags = output.Format.Tags
	return
}

func (st *Store) metadataFieldsFromFile(show string, id uint64) (map[string]interface{}, error) {
	duration, size, tags, err := st.readMetadataFromFile(st.GetFilePath(show, id))
	if err != nil {
		return nil, err
	}
	fields := make(map[string]interface{})
	fields["duration"] = duration.Seconds()
	fields["size"] = size

	// tag names are based on vorbis comments,
	// TODO: make this configurable and dependent on the storage format
	for tag, value := range tags {
		switch strings.ToLower(tag) {
		case "artist":
			fields["metadata__artist"] = value
		case "title":
			fields["metadata__title"] = value
		case "album":
			fields["metadata__album"] = value
		case "organization":
			fields["metadata__organization"] = value
		case "isrc":
			fields["metadata__isrc"] = value
		}
	}
	return fields, nil
}

func (st *Store) writeMetadataToFile(filename string, tags map[string]string) (uint64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()

	dir, name := filepath.Split(filename)
	newfile, err := os.CreateTemp(dir, name+".*")
	if err != nil {
		return 0, fmt.Errorf("failed to create temporary file: %v", err)
	}
	defer os.Remove(newfile.Name())
	defer newfile.Close()

	// cmd := exec.CommandContext(ctx, "ffmpeg", "-hide_banner", "-nostats", "-n", "-i", filename)
	cmd := exec.CommandContext(ctx, "ffmpeg", "-nostats", "-y", "-i", filename)
	for k, v := range tags {
		cmd.Args = append(cmd.Args, "-metadata", k+"="+v)
	}
	cmd.Args = append(cmd.Args, "-codec:a", "copy", "-vn")
	cmd.Args = append(cmd.Args, "-f", st.Audio.Format.String(), newfile.Name())

	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	if err = cmd.Run(); err != nil {
		if exiterr, ok := err.(*exec.ExitError); ok {
			if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
				// TODO: handle stderr?
				return 0, fmt.Errorf("ffmpeg returned %d", status.ExitStatus())
			}
		}
		return 0, fmt.Errorf("getting exit code of ffmpeg failed: %v", err)
	}

	fi, err := newfile.Stat()
	if err != nil {
		return 0, fmt.Errorf("failed to read new filesize: %v", err)
	}

	size := uint64(fi.Size())
	err = os.Rename(newfile.Name(), filename)
	return size, err
}

func (st *Store) metadataFieldsToFile(show string, id uint64, tags map[string]string) (map[string]interface{}, error) {
	// tag names are based on vorbis comments,
	// TODO: make this configurable and dependent on the storage format
	fields := make(map[string]interface{})
	for tag, value := range tags {
		switch strings.ToLower(tag) {
		case "artist":
			fields["metadata__artist"] = value
			tags[tag] = value
		case "title":
			fields["metadata__title"] = value
			tags[tag] = value
		case "album":
			fields["metadata__album"] = value
			tags[tag] = value
		case "organization":
			fields["metadata__organization"] = value
			tags[tag] = value
		case "isrc":
			fields["metadata__isrc"] = value
			tags[tag] = value
		default:
			return nil, ErrInvalidMetadataField(tag)
		}
	}

	size, err := st.writeMetadataToFile(st.GetFilePath(show, id), tags)
	if err != nil {
		return nil, err
	}
	fields["size"] = size
	return fields, nil
}
