//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package auth

import (
	"os"
	"time"
)

type AuthorizationInfo struct {
	Privileged  bool     `json:"privileged" yaml:"privileged" toml:"privileged"`
	ReadOnly    bool     `json:"readonly" yaml:"readonly" toml:"readonly"`
	AllShows    bool     `json:"all-shows" yaml:"all-shows" toml:"all-shows"`
	Shows       []string `json:"shows" yaml:"shows" toml:"shows"`
	PublicShows []string `json:"public-shows" yaml:"public-shows" toml:"public-shows"`
}

type StaticSessionConfig struct {
	Secret            string `json:"secret" yaml:"secret" toml:"secret"`
	AuthorizationInfo `yaml:",inline"`
}

type SessionsConfig struct {
	MaxAge time.Duration                   `json:"max-age" yaml:"max-age" toml:"max-age"`
	Static map[string]*StaticSessionConfig `json:"static" yaml:"static" toml:"static"`
}

type OIDCConfig struct {
	IssuerURL    string        `json:"issuer-url" yaml:"issuer-url" toml:"issuer-url"`
	ClientID     string        `json:"client-id" yaml:"client-id" toml:"client-id"`
	ClientSecret string        `json:"client-secret" yaml:"client-secret" toml:"client-secret"`
	CallbackURL  string        `json:"callback-url" yaml:"callback-url" toml:"callback-url"`
	LoginTimeout time.Duration `json:"login-timeout" yaml:"login-timeout" toml:"login-timeout"`
}

type PasswdUserConfig struct {
	Password          string `json:"password" yaml:"password" toml:"password"`
	AuthorizationInfo `yaml:",inline"`
}

type Config struct {
	Sessions SessionsConfig               `json:"sessions" yaml:"sessions" toml:"sessions"`
	OIDC     *OIDCConfig                  `json:"oidc" yaml:"oidc" toml:"oidc"`
	Passwd   map[string]*PasswdUserConfig `json:"passwd" yaml:"passwd" toml:"passwd"`
}

func (c *Config) ExpandEnv() {
	if c.Sessions.Static != nil {
		for name := range c.Sessions.Static {
			c.Sessions.Static[name].Secret = os.ExpandEnv(c.Sessions.Static[name].Secret)
		}
	}
	if c.OIDC != nil {
		c.OIDC.IssuerURL = os.ExpandEnv(c.OIDC.IssuerURL)
		c.OIDC.ClientID = os.ExpandEnv(c.OIDC.ClientID)
		c.OIDC.ClientSecret = os.ExpandEnv(c.OIDC.ClientSecret)
		c.OIDC.CallbackURL = os.ExpandEnv(c.OIDC.CallbackURL)
	}
	if c.Passwd != nil {
		for user := range c.Passwd {
			c.Passwd[user].Password = os.ExpandEnv(c.Passwd[user].Password)
		}
	}
}
