//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package auth

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"net/http"
	"sync/atomic"

	"github.com/gin-gonic/gin"
)

type contextKey int

var (
	sessionContextKey contextKey
)

func generateRandomString(len int) (string, error) {
	b := make([]byte, len)
	if _, err := rand.Read(b[:]); err != nil {
		return "", err
	}
	return base64.RawURLEncoding.EncodeToString(b[:]), nil
}

type BackendState uint32

const (
	BackendNew BackendState = iota
	BackendInitializing
	BackendReady
	BackendFailed
	BackendDestroyed
)

func (s *BackendState) set(state BackendState) {
	atomic.StoreUint32((*uint32)(s), uint32(state))
}

func (s *BackendState) get() (state BackendState) {
	return BackendState(atomic.LoadUint32((*uint32)(s)))
}

func (s *BackendState) String() string {
	switch s.get() {
	case BackendNew:
		return "new"
	case BackendInitializing:
		return "initializing"
	case BackendReady:
		return "ready"
	case BackendFailed:
		return "failed"
	case BackendDestroyed:
		return "destroyed"
	}
	return "unknown"
}

func (s *BackendState) MarshalText() (data []byte, err error) {
	data = []byte(s.String())
	return
}

type AuthBackendInfo struct {
	Name        string
	Description string
	State       *BackendState `swaggertype:"string" enums:"new,initializing,ready,failed,destroyed,unknown"`
}

type NewSessionRequest struct {
	Backend   string          `json:"backend"`
	Arguments json.RawMessage `json:"arguments"`
}

type NewSessionResponse struct {
	Session *Session `json:"session"`
	Token   string   `json:"token"`
}

type HTTPErrorResponse struct {
	Error string `json:"error,omitempty"`
}

func sendHTTPInvalidSessionResponse(c *gin.Context) {
	c.JSON(http.StatusUnauthorized, HTTPErrorResponse{"Request does not contain a valid token or session is already expired."})
}
