//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package auth

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
)

func TestMain(m *testing.M) {
	auth.infoLog = log.New(io.Discard, "", 0)
	auth.errLog = log.New(io.Discard, "", 0)
	auth.dbgLog = log.New(io.Discard, "", 0)
	gin.SetMode(gin.ReleaseMode)
	os.Exit(m.Run())
}

//
// Testing
//

func TestAuthDisabled(t *testing.T) {
	if err := Init(nil, nil, nil, nil); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	if auth.sessions != nil {
		t.Fatalf("authentication should be disabled but Init created a session manager")
	}

	router := gin.New()
	InstallHTTPHandler(&router.RouterGroup)

	req, err := http.NewRequest("GET", "/backends", nil)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if rr.Code != http.StatusNotFound {
		t.Fatalf("list backends should return %d when authentication is disabled but returened: %d (%s)", http.StatusNotFound, rr.Code, http.StatusText(rr.Code))
	}

	if req, err = http.NewRequest("GET", "/session", nil); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if rr.Code != http.StatusBadRequest {
		t.Fatalf("get session should return %d when authentication is disabled but returened: %d (%s)", http.StatusBadRequest, rr.Code, http.StatusText(rr.Code))
	}

	var sOut *Session
	router = gin.New()
	router.Use(Middleware())
	router.GET("/api/endpoint", func(c *gin.Context) {
		s, ok := SessionFromRequest(c.Request)
		if !ok {
			t.Fatalf("protected handler did not get a valid session")
		}
		sOut = s
	})

	if req, err = http.NewRequest("GET", "/api/endpoint", nil); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	if sOut != anonPrivileged {
		t.Fatalf("authentication middleware did not attach the correct session to the proteceted handler")
	}
}

func TestAuthNoOIDC(t *testing.T) {
	cfg := &Config{}
	if err := Init(cfg, nil, nil, nil); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	if auth.sessions == nil {
		t.Fatalf("authentication should be enabled but Init created no session manager")
	}

	router := gin.New()
	InstallHTTPHandler(&router.RouterGroup)

	req, err := http.NewRequest("GET", "/backends", nil)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	body := strings.TrimSpace(rr.Body.String())
	if rr.Code != http.StatusOK || body != "[]" {
		t.Fatalf("list backends should return %d and an empty list when OIDC is disabled but returned: %d (%s) and body='%s'", http.StatusOK, rr.Code, http.StatusText(rr.Code), body)
	}

	if req, err = http.NewRequest("GET", "/session", nil); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if rr.Code != http.StatusUnauthorized {
		t.Fatalf("get session should return %d when no 'Authorization' header is found but returned: %d (%s)", http.StatusUnauthorized, rr.Code, http.StatusText(rr.Code))
	}

	if req, err = http.NewRequest("POST", "/session", nil); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	req.Body = http.NoBody
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if rr.Code != http.StatusBadRequest {
		t.Fatalf("new session should return %d with empty body but returned: %d (%s)", http.StatusBadRequest, rr.Code, http.StatusText(rr.Code))
	}

	if req, err = http.NewRequest("POST", "/session", http.NoBody); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if rr.Code != http.StatusBadRequest {
		t.Fatalf("new session request with empty body should return %d but returned: %d (%s)", http.StatusBadRequest, rr.Code, http.StatusText(rr.Code))
	}

	testVectors := []struct {
		body string
		code int
	}{
		{"", http.StatusBadRequest},
		{"{}", http.StatusBadRequest},
		{"{\"key\":\"value\"}", http.StatusBadRequest},
		{"{\"backend\":\"invalid\"}", http.StatusBadRequest},
		{"{\"backend\":\"oidc\"}", http.StatusConflict}, // OIDC is disabled!
	}

	for _, vector := range testVectors {
		req, err := http.NewRequest("POST", "/session", strings.NewReader(vector.body))
		if err != nil {
			t.Fatalf("unexpected error: %v", err)
		}
		rr := httptest.NewRecorder()
		router.ServeHTTP(rr, req)
		if rr.Code != vector.code {
			t.Fatalf("new session request with body '%s' should return %d but returned: %d (%s)", vector.body, vector.code, rr.Code, http.StatusText(rr.Code))
		}
	}
}

// TODO: implement this - we need a working OIDC IDP for this....
// func TestAuthWidhOIDC(t *testing.T) {
// }

func TestAuthBearerToken(t *testing.T) {
	cfg := &Config{}
	if err := Init(cfg, nil, nil, nil); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	if auth.sessions == nil {
		t.Fatalf("authentication should be enabled but Init created no session manager")
	}

	s, err := auth.sessions.new()
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	router := gin.New()
	InstallHTTPHandler(&router.RouterGroup)

	testVectors := []struct {
		token string
		code  int
	}{
		{"", http.StatusUnauthorized},
		{"Basic", http.StatusUnauthorized},
		{"Bearer", http.StatusUnauthorized},
		{"Bearer ", http.StatusUnauthorized},
		{"Bearer invalid", http.StatusUnauthorized},
		{"Bearer invalid:", http.StatusUnauthorized},
		{"Bearer unknown:secret", http.StatusUnauthorized},
		{"Bearer " + s.id + ":invalid", http.StatusUnauthorized},
		{"Bearer " + s.getToken(), http.StatusOK},
	}

	for _, vector := range testVectors {
		req, err := http.NewRequest("GET", "/session", nil)
		if err != nil {
			t.Fatalf("unexpected error: %v", err)
		}
		req.Header.Set("Authorization", vector.token)
		rr := httptest.NewRecorder()
		router.ServeHTTP(rr, req)
		if rr.Code != vector.code {
			t.Fatalf("get session should return %d when 'Authorization' header '%s' is found but returned: %d (%s)", vector.code, vector.token, rr.Code, http.StatusText(rr.Code))
		}
	}
}

func TestAuthWaitForLogin(t *testing.T) {
	cfg := &Config{}
	if err := Init(cfg, nil, nil, nil); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	if auth.sessions == nil {
		t.Fatalf("authentication should be enabled but Init created no session manager")
	}

	s, err := auth.sessions.new()
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	router := gin.New()
	InstallHTTPHandler(&router.RouterGroup)

	timeout := time.AfterFunc(time.Second, func() {
		t.Fatalf("waiting for session state change does not respect the request context")
	})

	req, err := http.NewRequest("GET", "/session?wait-for-login=1", nil)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	req = req.WithContext(ctx)
	req.Header.Set("Authorization", "Bearer "+s.getToken())
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if rr.Code != http.StatusRequestTimeout {
		t.Fatalf("unexpected reponse code: %d (%s)", rr.Code, http.StatusText(rr.Code))
	}
	cancel()
	timeout.Stop()

	for _, state := range []SessionState{SessionStateLoggedIn, SessionStateLoginFailed, SessionStateLoginTimeout} {
		s.setState(SessionStateNew)
		time.AfterFunc(100*time.Millisecond, func() {
			s.setState(state)
		})

		req, err = http.NewRequest("GET", "/session?wait-for-login=1", nil)
		if err != nil {
			t.Fatalf("unexpected error: %v", err)
		}
		ctx, cancel = context.WithTimeout(context.Background(), time.Second)
		req = req.WithContext(ctx)
		req.Header.Set("Authorization", "Bearer "+s.getToken())
		rr = httptest.NewRecorder()
		router.ServeHTTP(rr, req)
		if rr.Code != http.StatusOK {
			t.Fatalf("unexpected reponse code: %d (%s)", rr.Code, http.StatusText(rr.Code))
		}
		cancel()

		result := make(map[string]interface{})
		if err = json.NewDecoder(rr.Body).Decode(&result); err != nil {
			t.Fatalf("unexpected error: %v", err)
		}
		if st, ok := result["state"]; !ok {
			t.Fatalf("resulting session does not contain the state")
		} else if st != state.String() {
			t.Fatalf("state of resulting session is wrong, expected: %s, got %v", state.String(), st)
		}
	}
}

func TestAuthDeleteSession(t *testing.T) {
	cfg := &Config{}
	if err := Init(cfg, nil, nil, nil); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	if auth.sessions == nil {
		t.Fatalf("authentication should be enabled but Init created no session manager")
	}

	s, err := auth.sessions.new()
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	router := gin.New()
	InstallHTTPHandler(&router.RouterGroup)

	req, err := http.NewRequest("DELETE", "/session", nil)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if rr.Code != http.StatusUnauthorized {
		t.Fatalf("unexpected reponse code: %d (%s)", rr.Code, http.StatusText(rr.Code))
	}

	if req, err = http.NewRequest("DELETE", "/session", nil); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	req.Header.Set("Authorization", "Bearer "+s.getToken())
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if rr.Code != http.StatusOK {
		t.Fatalf("unexpected reponse code: %d (%s)", rr.Code, http.StatusText(rr.Code))
	}

	sOut := auth.sessions.get(s.ID())
	if sOut != nil && sOut.State() == SessionStateLoggedIn {
		t.Fatalf("session is still logged in")
	}
}

func TestAuthMiddleware(t *testing.T) {
	cfg := &Config{}
	if err := Init(cfg, nil, nil, nil); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	if auth.sessions == nil {
		t.Fatalf("authentication should be enabled but Init created no session manager")
	}

	s, err := auth.sessions.new()
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	var sOut *Session
	router := gin.New()
	router.Use(Middleware())
	router.GET("/api/endpoint", func(c *gin.Context) {
		sOut, _ = SessionFromRequest(c.Request)
	})

	req, err := http.NewRequest("GET", "/api/endpoint", nil)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	// no Authorization Header
	if rr.Code != http.StatusUnauthorized {
		t.Fatalf("unexpected reponse code: %d (%s)", rr.Code, http.StatusText(rr.Code))
	}

	if req, err = http.NewRequest("GET", "/api/endpoint", nil); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	req.Header.Set("Authorization", "Bearer "+s.getToken())
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	// session not logged in
	if rr.Code != http.StatusUnauthorized {
		t.Fatalf("unexpected reponse code: %d (%s)", rr.Code, http.StatusText(rr.Code))
	}

	s.Username = "foo"
	s.setState(SessionStateLoggedIn)

	if req, err = http.NewRequest("GET", "/api/endpoint", nil); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	req.Header.Set("Authorization", "Bearer "+s.getToken())
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	if rr.Code != http.StatusOK {
		t.Fatalf("unexpected reponse code: %d (%s)", rr.Code, http.StatusText(rr.Code))
	}
	if sOut != s {
		t.Fatalf("authentication middleware did not attach the correct session to the proteceted handler")
	}
}
