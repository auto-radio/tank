//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package auth

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

const (
	defaultAge     = 24 * time.Hour
	tokenSeperator = ':'
)

type SessionState uint32

const (
	SessionStateNew SessionState = iota
	SessionStateStale
	SessionStateLoginStarted
	SessionStateLoginFinalizing
	SessionStateLoggedIn
	SessionStateLoginFailed
	SessionStateLoginTimeout
	SessionStateLoggedOut
	SessionStateRemoved
)

func (s SessionState) String() string {
	switch s {
	case SessionStateNew:
		return "new"
	case SessionStateStale:
		return "stale"
	case SessionStateLoginStarted:
		return "login-started"
	case SessionStateLoginFinalizing:
		return "login-finalizing"
	case SessionStateLoggedIn:
		return "logged-in"
	case SessionStateLoginFailed:
		return "login-failed"
	case SessionStateLoginTimeout:
		return "login-timeout"
	case SessionStateLoggedOut:
		return "logged-out"
	case SessionStateRemoved:
		return "removed"
	}
	return "unknown"
}

func (s SessionState) MarshalText() (data []byte, err error) {
	data = []byte(s.String())
	return
}

type Session struct {
	isStatic bool
	id       string
	secret   string
	state    SessionState
	ctx      context.Context
	cancel   context.CancelFunc

	oidc *OIDCSession

	subs      chan struct{}
	subsMutex sync.Mutex

	Username string `json:"username"`
	AuthorizationInfo
}

var (
	anonPrivileged = &Session{Username: "anonymous", AuthorizationInfo: AuthorizationInfo{Privileged: true, ReadOnly: false, AllShows: true, Shows: []string{}}}
	anonAllowNone  = &Session{Username: "anonymous", AuthorizationInfo: AuthorizationInfo{Privileged: false, ReadOnly: false, AllShows: false, Shows: []string{}}}
	anonAllowAll   = &Session{Username: "anonymous", AuthorizationInfo: AuthorizationInfo{Privileged: false, ReadOnly: false, AllShows: true, Shows: []string{}}}
	anonAllowAllRO = &Session{Username: "anonymous", AuthorizationInfo: AuthorizationInfo{Privileged: false, ReadOnly: true, AllShows: true, Shows: []string{}}}
)

func (s *Session) emptyCopy() *Session {
	sc := &Session{}
	sc.id = s.id
	sc.secret = s.secret
	sc.state = s.State()
	sc.ctx = s.ctx
	sc.cancel = s.cancel

	sc.oidc = s.oidc

	return sc
}

func (s *Session) ID() string {
	return s.id
}

func (s *Session) State() SessionState {
	return SessionState(atomic.LoadUint32((*uint32)(&s.state)))
}

func (s *Session) setState(st SessionState) {
	old := atomic.SwapUint32((*uint32)(&s.state), uint32(st))
	if old != uint32(st) {
		s.signalSubscribers()
	}
}

func (s *Session) updateState(old, new SessionState) bool {
	ok := atomic.CompareAndSwapUint32((*uint32)(&s.state), uint32(old), uint32(new))
	if ok && old != new {
		s.signalSubscribers()
	}
	return ok
}

func (s *Session) expire() {
	if s.cancel != nil {
		s.cancel()
	}
}

func (s *Session) Expired() bool {
	select {
	case <-s.ctx.Done():
		return true
	default:
	}
	return false
}

func (s *Session) subscribe() <-chan struct{} {
	s.subsMutex.Lock()
	defer s.subsMutex.Unlock()

	if s.subs == nil {
		s.subs = make(chan struct{})
	}
	return s.subs
}

func (s *Session) signalSubscribers() {
	s.subsMutex.Lock()
	defer s.subsMutex.Unlock()

	if s.subs != nil {
		close(s.subs)
		s.subs = nil
	}
}

func (s *Session) MarshalJSON() ([]byte, error) {
	data := struct {
		ID          string       `json:"id"`
		State       SessionState `json:"state"`
		Expires     *time.Time   `json:"expires,omitempty"`
		Username    string       `json:"username"`
		Privileged  bool         `json:"privileged"`
		ReadOnly    bool         `json:"readonly"`
		AllShows    bool         `json:"all-shows"`
		Shows       []string     `json:"shows,omitempty"`
		PublicShows []string     `json:"public-shows,omitempty"`
		OIDC        *OIDCSession `json:"oidc,omitempty"`
	}{
		ID:          s.id,
		State:       s.State(),
		Username:    s.Username,
		Privileged:  s.Privileged,
		ReadOnly:    s.ReadOnly,
		AllShows:    s.AllShows,
		Shows:       s.Shows,
		PublicShows: s.PublicShows,
	}
	if deadline, ok := s.ctx.Deadline(); ok {
		data.Expires = &deadline
	}
	// this should only be enabled for debug purposes
	// if data.State == SessionStateLoggedIn {
	// 	data.OIDC = s.oidc
	// }
	return json.Marshal(data)
}

func (s *Session) getToken() string {
	return s.id + string(tokenSeperator) + s.secret
}

func parseBearerAuthHeader(authHeader string) (string, string, bool) {
	const prefix = "Bearer "

	if len(authHeader) < len(prefix) || !strings.EqualFold(authHeader[:len(prefix)], prefix) {
		return "", "", false
	}
	token := authHeader[len(prefix):]
	if token == "" {
		return "", "", false
	}
	sepIdx := strings.IndexByte(token, tokenSeperator)
	if sepIdx < 0 {
		return "", "", false
	}
	return token[:sepIdx], token[sepIdx+1:], true
}

func getSessionFromBearerToken(r *http.Request) *Session {
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		return nil
	}

	sID, secret, ok := parseBearerAuthHeader(authHeader)
	if !ok {
		return nil
	}

	s := auth.sessions.get(sID)
	if s == nil || s.secret != secret || s.Expired() {
		return nil
	}

	return s
}

func attachSessionToRequest(r *http.Request, s *Session) *http.Request {
	ctx := context.WithValue(r.Context(), sessionContextKey, s)
	return r.WithContext(ctx)
}

func SessionFromRequest(r *http.Request) (*Session, bool) {
	s, ok := r.Context().Value(sessionContextKey).(*Session)
	return s, ok
}

type SessionManager struct {
	mutex    sync.RWMutex
	maxAge   time.Duration
	sessions map[string]*Session
}

func NewSessionManager(c SessionsConfig) (sm *SessionManager, err error) {
	sm = &SessionManager{maxAge: defaultAge}
	if c.MaxAge > 0 {
		sm.maxAge = c.MaxAge
	}
	sm.sessions = make(map[string]*Session)
	if c.Static != nil {
		for name, value := range c.Static {
			s := &Session{id: name, secret: value.Secret, state: SessionStateLoggedIn}
			s.isStatic = true
			s.ctx = context.Background()
			s.Username = name
			s.ReadOnly = value.ReadOnly
			s.AllShows = value.AllShows
			s.Shows = value.Shows
			sm.sessions[s.id] = s
		}
	}

	go sm.runMaintenance()
	return
}

func (sm *SessionManager) runMaintenance() {
	t := time.NewTicker(10 * time.Second) // TODO: hardcoded value
	for {
		<-t.C
		sm.cleanup()
	}
}

func (sm *SessionManager) new() (s *Session, err error) {
	sm.mutex.Lock()
	defer sm.mutex.Unlock()

	s = &Session{}
	if s.id, err = generateRandomString(16); err != nil {
		return
	}
	if s.secret, err = generateRandomString(32); err != nil {
		return
	}
	s.state = SessionStateNew
	s.ctx, s.cancel = context.WithTimeout(context.Background(), sm.maxAge)
	sm.sessions[s.id] = s
	auth.dbgLog.Printf("authentication: added new session %s", s.id)
	return
}

func (sm *SessionManager) get(id string) *Session {
	sm.mutex.RLock()
	defer sm.mutex.RUnlock()

	s, ok := sm.sessions[id]
	if !ok {
		return nil
	}
	return s
}

func (sm *SessionManager) getAndSubscribe(id string) (*Session, <-chan struct{}) {
	sm.mutex.RLock()
	defer sm.mutex.RUnlock()

	s, ok := sm.sessions[id]
	if !ok {
		return nil, nil
	}
	return s, s.subscribe()
}

func (sm *SessionManager) update(s *Session) error {
	sm.mutex.Lock()
	defer sm.mutex.Unlock()

	old, ok := sm.sessions[s.id]
	if !ok {
		return errors.New("session not found")
	}
	if s.id != old.id || s.secret != old.secret || s.ctx != old.ctx { // TODO: function compares don't work: s.cancel != old.cancel
		panic("sessions.update(): id, secret, ctx and cancel need not change!")
	}

	old.signalSubscribers()

	sm.sessions[s.id] = s
	old.setState(SessionStateStale)
	auth.dbgLog.Printf("authentication: updated session %s", s.id)
	return nil
}

func (sm *SessionManager) cleanup() {
	sm.mutex.Lock()
	defer sm.mutex.Unlock()

	for id, s := range sm.sessions {
		expired := s.Expired()
		st := s.State()
		if expired || st > SessionStateLoggedIn {
			delete(sm.sessions, id)
			s.setState(SessionStateRemoved)
			s.expire()
			reason := st.String()
			if expired {
				reason = "expired/canceled"
			}
			auth.dbgLog.Printf("authentication: removed session %s (reason=%s)", id, reason)
		}
	}
}
