//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package auth

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	oidc "github.com/coreos/go-oidc"
	"github.com/gin-gonic/gin"
	"golang.org/x/oauth2"
)

const (
	defaultLoginTimeout = 5 * time.Minute
)

type OIDCSession struct {
	backend     *OIDCBackend
	nonce       string
	tokenSource oauth2.TokenSource
}

func (s *OIDCSession) refresh(ctx context.Context, in *Session) (*Session, error) {
	userInfo, err := s.backend.provider.UserInfo(ctx, s.tokenSource)
	if err != nil {
		return nil, errors.New("fetching OIDC UserInfo failed: " + err.Error())
	}

	out := in.emptyCopy()
	if err := userInfo.Claims(out); err != nil {
		return nil, errors.New("parsing OIDC UserInfo failed: " + err.Error())
	}
	if out.Privileged {
		out.ReadOnly = false
		out.AllShows = true
	}
	if err = auth.sessions.update(out); err != nil {
		return nil, errors.New("updating session failed: " + err.Error())
	}
	return out, nil
}

// TODO this needs more testing!!
func runRefresher(id string) {
	ticker := time.NewTicker(15 * time.Second) // TODO: hardcoded value
	defer ticker.Stop()
	errCnt := 0

	s, subCh := auth.sessions.getAndSubscribe(id)
	if s == nil {
		auth.errLog.Printf("authentication/oidc: failed to start refresher for session: %s", id)
		return
	}

	auth.dbgLog.Printf("authentication/oidc: successfully started refresher for session: %s", id)
	defer auth.dbgLog.Printf("authentication/oidc: stopped refresher for session: %s", id)

	for {
		select {
		case <-subCh:
			if s, subCh = auth.sessions.getAndSubscribe(id); s == nil {
				return
			}
		case <-s.ctx.Done():
			return
		case <-ticker.C:
			ctx, cancel := context.WithTimeout(s.ctx, time.Minute) // TODO: hardcoded value
			newS, err := s.oidc.refresh(ctx, s)
			cancel()
			if err != nil {
				errCnt++
				if errCnt > 3 { // TODO: hardcoded value
					auth.errLog.Printf("authentication/oidc: refreshing session %s has failed %d times: %v, will be logged out", id, errCnt, err)
					s.setState(SessionStateLoggedOut)
					return
				}
				auth.errLog.Printf("authentication/oidc: refreshing session %s has failed: %v, will retry...", id, err)
			} else {
				errCnt = 0
				s = newS
			}
		}
	}
}

func loginTimeout(s *Session) {
	if s.updateState(SessionStateNew, SessionStateLoginTimeout) {
		return
	}
	if s.updateState(SessionStateLoginStarted, SessionStateLoginTimeout) {
		return
	}
	if s.updateState(SessionStateLoginFinalizing, SessionStateLoginTimeout) {
		return
	}
}

// This is only safe when session is logged in!
func (s *OIDCSession) MarshalJSON() ([]byte, error) {
	t, err := s.tokenSource.Token()
	if err != nil {
		return nil, err
	}
	return json.Marshal(struct {
		Token *oauth2.Token `json:"token,omitempty"`
	}{
		Token: t,
	})
}

type OIDCBackend struct {
	loginTimeout time.Duration
	issuerURL    string
	provider     *oidc.Provider
	verifier     *oidc.IDTokenVerifier
	oauth2Config oauth2.Config
	state        *BackendState
}

func NewOIDCBackend(cfg *OIDCConfig) (b *OIDCBackend, err error) {
	// TODO: make ctx a parameter?
	b = &OIDCBackend{issuerURL: cfg.IssuerURL, loginTimeout: cfg.LoginTimeout}
	if b.loginTimeout <= 0 {
		b.loginTimeout = defaultLoginTimeout
	}
	state := BackendNew
	b.state = &state

	go b.initialize(cfg)
	return
}

func (b *OIDCBackend) initialize(cfg *OIDCConfig) {
	b.state.set(BackendInitializing)
	for {
		var err error
		ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second) // TODO: hardcoded value
		b.provider, err = oidc.NewProvider(ctx, cfg.IssuerURL)
		cancel()
		if err == nil {
			break
		}
		auth.errLog.Printf("authentication/oidc: initialization failed: %v, will retry...", err)
		time.Sleep(10 * time.Second) // TODO: hardcoded value
	}

	oidcConfig := &oidc.Config{
		ClientID: cfg.ClientID,
	}
	b.verifier = b.provider.Verifier(oidcConfig)

	b.oauth2Config = oauth2.Config{
		ClientID:     cfg.ClientID,
		ClientSecret: cfg.ClientSecret,
		Endpoint:     b.provider.Endpoint(),
		RedirectURL:  cfg.CallbackURL,
		Scopes:       []string{oidc.ScopeOpenID, "username", "aura_shows"},
	}

	b.state.set(BackendReady)
	auth.infoLog.Printf("authentication/oidc: initialization complete, backend is now %s", b.state.String())
}

func (b *OIDCBackend) String() string {
	return fmt.Sprintf("OpenID Connect using Identity Provider: %s", b.issuerURL)
}

func (b *OIDCBackend) NewSession(ctx context.Context, arguments json.RawMessage) (s *Session, err error) {
	if b.state.get() != BackendReady {
		return nil, errors.New("oidc backend is not ready")
	}

	var oauth2Token *oauth2.Token
	if arguments != nil {
		oauth2Token = &oauth2.Token{}
		if err = json.Unmarshal(arguments, oauth2Token); err != nil {
			return nil, errors.New("failed to parse Oauth2 Token: " + err.Error())
		}
	}

	os := &OIDCSession{backend: b}
	if os.nonce, err = generateRandomString(16); err != nil {
		return
	}

	if s, err = auth.sessions.new(); err != nil {
		return nil, err
	}
	s.oidc = os // TODO: setting this directly is probably fine but rather ugly...

	if oauth2Token != nil {
		os.tokenSource = b.oauth2Config.TokenSource(s.ctx, oauth2Token)
		bakS := s
		if s, err = os.refresh(ctx, s); err != nil {
			bakS.setState(SessionStateLoginFailed)
			return
		}
		s.setState(SessionStateLoggedIn)

		go runRefresher(s.ID())
	} else {
		time.AfterFunc(b.loginTimeout, func() { loginTimeout(s) })
	}
	return
}

// Login creates a session via OIDC.
// @Summary      Create OIDC session
// @Description  Creates a session via OIDC. Redirects to identity provider.
// @Produce      json
// @Param        session-id  query  string  true  "OIDC session ID"
// @Success      302
// @Failure      400  {object}  HTTPErrorResponse
// @Failure      401  {object}  HTTPErrorResponse
// @Failure      409  {object}  HTTPErrorResponse
// @Router       /auth/oidc/login [get]
func (b *OIDCBackend) Login(c *gin.Context) {
	s := auth.sessions.get(c.Query("session-id"))
	if s == nil {
		sendHTTPInvalidSessionResponse(c)
		return
	}
	if s.oidc == nil {
		c.JSON(http.StatusConflict, HTTPErrorResponse{"this is not an OIDC session"})
		return
	}

	if !s.updateState(SessionStateNew, SessionStateLoginStarted) {
		c.JSON(http.StatusConflict, HTTPErrorResponse{"this session is already logged in or in an invalid state"})
		return
	}

	c.Redirect(http.StatusFound, b.oauth2Config.AuthCodeURL(s.ID(), oidc.Nonce(s.oidc.nonce)))
}

// Callback completes OIDC login.
// @Summary      Complete OIDC login
// @Description  Completes OIDC login.
// @Produce      json
// @Param        state  query  string  true  "OIDC state"
// @Param        code   query  string  true  "OIDC code"
// @Success      200  {object}  string
// @Failure      400  {object}  HTTPErrorResponse
// @Failure      401  {object}  HTTPErrorResponse
// @Failure      409  {object}  HTTPErrorResponse
// @Failure      410  {object}  HTTPErrorResponse
// @Failure      500  {object}  HTTPErrorResponse
// @Router       /auth/oidc/callback [get]
func (b *OIDCBackend) Callback(c *gin.Context) {
	s := auth.sessions.get(c.Query("state"))
	if s == nil {
		sendHTTPInvalidSessionResponse(c)
		return
	}
	if s.oidc == nil {
		c.JSON(http.StatusConflict, HTTPErrorResponse{"this is not an OIDC session"})
		return
	}

	// TODO: handle login error (query parameter: error, error_description)

	if !s.updateState(SessionStateLoginStarted, SessionStateLoginFinalizing) {
		c.JSON(http.StatusConflict, HTTPErrorResponse{"this session is already logged in or in an invalid state"})
		return
	}

	oauth2Token, err := b.oauth2Config.Exchange(c.Request.Context(), c.Query("code"))
	if err != nil {
		s.setState(SessionStateLoginFailed)
		c.JSON(http.StatusBadRequest, HTTPErrorResponse{"OAuth2 token exchange failed: " + err.Error()})
		return
	}
	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		s.setState(SessionStateLoginFailed)
		c.JSON(http.StatusBadRequest, HTTPErrorResponse{"OIDC verification failed: no id_token field in oauth2 token."})
		return
	}

	// Verify the ID Token signature and nonce.
	idToken, err := b.verifier.Verify(c.Request.Context(), rawIDToken)
	if err != nil {
		s.setState(SessionStateLoginFailed)
		c.JSON(http.StatusInternalServerError, HTTPErrorResponse{"OAuth2 ID token verification failed: " + err.Error()})
		return
	}
	if idToken.Nonce != s.oidc.nonce {
		s.setState(SessionStateLoginFailed)
		c.JSON(http.StatusInternalServerError, HTTPErrorResponse{"OAuth2 ID token verification failed: invalid nonce"})
		return
	}

	// Populate a new session with data from UserInfo endpoint.
	s.oidc.tokenSource = b.oauth2Config.TokenSource(s.ctx, oauth2Token)
	sOrig := s
	if s, err = s.oidc.refresh(c.Request.Context(), sOrig); err != nil {
		sOrig.setState(SessionStateLoginFailed)
		c.JSON(http.StatusInternalServerError, HTTPErrorResponse{err.Error()})
		return
	}
	if !s.updateState(SessionStateLoginFinalizing, SessionStateLoggedIn) {
		c.JSON(http.StatusGone, HTTPErrorResponse{"session login timeout"})
		return
	}
	go runRefresher(s.ID())

	c.JSON(http.StatusOK, "You are now logged in as: "+s.Username)
}
