//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package auth

import (
	"testing"
	"time"
)

var (
	testSecret = "secrettest"
)

//
// Testing
//

func TestCreateSessionManager(t *testing.T) {
	cfg := SessionsConfig{}
	if _, err := NewSessionManager(cfg); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
}

func TestSessionNewAndUpdate(t *testing.T) {
	sm, err := NewSessionManager(SessionsConfig{})
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	s1, err := sm.new()
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	if err = sm.update(&Session{id: "unknown"}); err == nil {
		t.Fatalf("updating non-existing session should fail")
	}

	s2 := s1.emptyCopy()
	s2.Username = "foo"

	if err = sm.update(s2); err != nil {
		t.Fatalf("updating session failed: %v", err)
	}
}

func TestSessionCleanup(t *testing.T) {
	sm, err := NewSessionManager(SessionsConfig{})
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	s1, err := sm.new()
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	if !s1.updateState(SessionStateNew, SessionStateLoggedOut) {
		t.Fatalf("updating session state should have worked")
	}

	s2, err := sm.new()
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	s3, err := sm.new()
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	s3.cancel()

	sm.cleanup()

	if sm.get(s1.id) != nil || sm.get(s3.id) != nil {
		t.Fatalf("dead sessions should have been cleaned up")
	}

	if sm.get(s2.id) != s2 {
		t.Fatalf("alive sessions shouldn't have been cleaned up")
	}
}

func TestSessionExpiry(t *testing.T) {
	cfg := SessionsConfig{}
	cfg.MaxAge = time.Second
	sm, err := NewSessionManager(cfg)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	s1, err := sm.new()
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	s2 := sm.get(s1.ID())
	if s2 == nil {
		t.Fatalf("session-manager returned no session")
	}
	if s2.Expired() {
		t.Fatalf("session has already expired")
	}

	if s1.id != s2.id {
		t.Fatalf("sessions don't match")
	}

	time.Sleep(time.Second + 100*time.Millisecond)

	if !s2.Expired() {
		t.Fatalf("session hasn't expired in time")
	}
}
