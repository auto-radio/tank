//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package auth

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
)

type PasswdBackend struct {
	userDB map[string]*PasswdUserConfig
	state  *BackendState
}

func NewPasswdBackend(userDB map[string]*PasswdUserConfig) (b *PasswdBackend, err error) {
	b = &PasswdBackend{userDB: userDB}
	state := BackendReady
	b.state = &state
	return
}

func (b *PasswdBackend) String() string {
	return fmt.Sprintf("Password-Based Authentication (%d users)", len(b.userDB))
}

func (b *PasswdBackend) NewSession(ctx context.Context, arguments json.RawMessage) (s *Session, err error) {
	if b.state.get() != BackendReady {
		return nil, errors.New("password backend is not ready")
	}

	if arguments == nil {
		return nil, errors.New("mandatory arguments missing")
	}

	args := struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}{}
	if err = json.Unmarshal(arguments, &args); err != nil {
		return nil, errors.New("failed to parse arguments: " + err.Error())
	}
	if args.Username == "" || args.Password == "" {
		return nil, errors.New("mandatory arguments missing")
	}

	user, exists := b.userDB[args.Username]
	if !exists || user.Password != args.Password {
		return nil, errors.New("invalid username and or password")
	}

	if s, err = auth.sessions.new(); err != nil {
		return nil, err
	}
	// TODO: setting this directly is probably fine but rather ugly...
	s.Username = args.Username
	s.Privileged = user.Privileged
	s.ReadOnly = user.ReadOnly
	s.AllShows = user.AllShows
	s.Shows = user.Shows

	s.setState(SessionStateLoggedIn)
	return
}
