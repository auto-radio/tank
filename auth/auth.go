//
//  tank, Import and Playlist Daemon for Aura project
//  Copyright (C) 2017-2020 Christian Pointner <equinox@helsinki.at>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

package auth

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

var auth = &Auth{}

type Auth struct {
	sessions *SessionManager
	backends struct {
		oidc   *OIDCBackend
		passwd *PasswdBackend
	}
	infoLog *log.Logger
	errLog  *log.Logger
	dbgLog  *log.Logger
}

func Init(c *Config, infoLog, errLog, dbgLog *log.Logger) (err error) {
	if infoLog == nil {
		infoLog = log.New(io.Discard, "", 0)
	}
	if errLog == nil {
		errLog = log.New(io.Discard, "", 0)
	}
	if dbgLog == nil {
		dbgLog = log.New(io.Discard, "", 0)
	}

	auth.infoLog = infoLog
	auth.errLog = errLog
	auth.dbgLog = dbgLog
	if c == nil {
		auth.infoLog.Println("authentication: disabled - will allow any request!")
		return
	}

	if auth.sessions, err = NewSessionManager(c.Sessions); err != nil {
		auth.errLog.Printf("authentication: failed to initialize session manager: %v", err)
		return
	}

	if c.OIDC != nil {
		if auth.backends.oidc, err = NewOIDCBackend(c.OIDC); err != nil {
			auth.errLog.Printf("authentication: failed to enable OIDC backend: %v", err)
			return
		}
		auth.infoLog.Printf("authentication: enabled %s", auth.backends.oidc)
	}

	if c.Passwd != nil {
		if auth.backends.passwd, err = NewPasswdBackend(c.Passwd); err != nil {
			auth.errLog.Printf("authentication: failed to enable passwd backend: %v", err)
			return
		}
		auth.infoLog.Printf("authentication: enabled %s", auth.backends.passwd)
	}

	auth.infoLog.Println("authentication: enabled")
	return
}

// newSession creates a new session.
// @Summary      Create session
// @Description  Creates a new session.
// @Accept      json
// @Produce      json
// @Param  request  body  NewSessionRequest  true  "Request data according to backend"
// @Success      200  {object}  NewSessionResponse
// @Failure      400  {object}  HTTPErrorResponse
// @Failure      409  {object}  HTTPErrorResponse
// @Router       /auth/session [post]
func newSession(c *gin.Context) {
	request := &NewSessionRequest{}
	err := json.NewDecoder(c.Request.Body).Decode(request)
	if err != nil {
		c.JSON(http.StatusBadRequest, HTTPErrorResponse{"Error decoding request: " + err.Error()})
		return
	}

	response := &NewSessionResponse{}
	switch strings.ToLower(request.Backend) {
	case "oidc":
		if auth.backends.oidc == nil {
			c.JSON(http.StatusConflict, HTTPErrorResponse{"OIDC authentication is not enabled"})
			return
		}
		s, err := auth.backends.oidc.NewSession(c.Request.Context(), request.Arguments)
		if err != nil {
			c.JSON(http.StatusBadRequest, HTTPErrorResponse{"Error creating session: " + err.Error()})
			return
		}
		response.Session = s
		response.Token = s.getToken()
	case "passwd":
		if auth.backends.passwd == nil {
			c.JSON(http.StatusConflict, HTTPErrorResponse{"Password-based authentication is not enabled"})
			return
		}
		s, err := auth.backends.passwd.NewSession(c.Request.Context(), request.Arguments)
		if err != nil {
			c.JSON(http.StatusBadRequest, HTTPErrorResponse{"Error creating session: " + err.Error()})
			return
		}
		response.Session = s
		response.Token = s.getToken()
	default:
		c.JSON(http.StatusBadRequest, HTTPErrorResponse{"invalid authentication backend: '" + request.Backend + "'"})
		return
	}
	c.JSON(http.StatusOK, response)
}

// getSession retrieves session info.
// @Summary      Retrieve session info
// @Description  Retrieves session info.
// @Produce      json
// @Success      200  {object}  Session
// @Failure      400  {object}  HTTPErrorResponse
// @Failure      404  {object}  HTTPErrorResponse
// @Failure      408  {object}  HTTPErrorResponse
// @Router       /auth/session [get]
func getSession(c *gin.Context) {
	s := getSessionFromBearerToken(c.Request)
	if s == nil {
		sendHTTPInvalidSessionResponse(c)
		return
	}

	if c.Query("wait-for-login") == "" {
		c.JSON(http.StatusOK, s)
		return
	}

	ctx := c.Request.Context()
	for {
		s, sub := auth.sessions.getAndSubscribe(s.ID())
		if s == nil {
			c.JSON(http.StatusNotFound, HTTPErrorResponse{"this session does no longer exist"})
			return
		}
		st := s.State()
		if st >= SessionStateLoggedIn {
			c.JSON(http.StatusOK, s)
			return
		}
		select {
		case <-sub:
		case <-ctx.Done():
			c.JSON(http.StatusRequestTimeout, HTTPErrorResponse{"error while waiting for state change: " + ctx.Err().Error()})
			return
		}
	}
}

// deleteSession deletes the session.
// @Summary      Delete session
// @Description  Deletes the session.
// @Produce      json
// @Success      200  {object}  string
// @Failure      400  {object}  HTTPErrorResponse
// @Failure      401  {object}  HTTPErrorResponse
// @Router       /auth/session [delete]
func deleteSession(c *gin.Context) {
	s := getSessionFromBearerToken(c.Request)
	if s == nil || s.State() > SessionStateLoggedIn {
		sendHTTPInvalidSessionResponse(c)
		return
	}
	if s.isStatic {
		c.JSON(http.StatusUnauthorized, HTTPErrorResponse{"static sessions cannot be logged-out."})
		return
	}
	s.setState(SessionStateLoggedOut)
	c.JSON(http.StatusOK, "you are now logged out")
}

// listBackends lists authentication backends.
// @Summary      List authentication backends
// @Description  Lists authentication backends.
// @Produce      json
// @Success      200  {object}  []AuthBackendInfo
// @Failure      404  {object}  HTTPErrorResponse
// @Router       /auth/backends [get]
func listBackends(c *gin.Context) {
	backends := []AuthBackendInfo{}
	if auth.backends.oidc != nil {
		backend := AuthBackendInfo{Name: "oidc"}
		backend.Description = auth.backends.oidc.String()
		backend.State = auth.backends.oidc.state
		backends = append(backends, backend)
	}
	if auth.backends.passwd != nil {
		backend := AuthBackendInfo{Name: "passwd"}
		backend.Description = auth.backends.passwd.String()
		backend.State = auth.backends.passwd.state
		backends = append(backends, backend)
	}

	c.JSON(http.StatusOK, backends)
}

func disabled(c *gin.Context) {
	code := http.StatusBadRequest
	if c.Param("path") == "/backends" {
		code = http.StatusNotFound
	}
	c.JSON(code, HTTPErrorResponse{"authentication is disabled"})
}

func InstallHTTPHandler(r *gin.RouterGroup) {
	if auth.sessions == nil {
		r.Any("/*path", disabled)
		return
	}

	backends := r.Group("backends")
	backends.GET("", listBackends)

	session := r.Group("session")
	session.POST("", newSession)
	session.GET("", getSession)
	session.DELETE("", deleteSession)

	if auth.backends.oidc != nil {
		oidc := r.Group("oidc")
		oidc.GET("login", auth.backends.oidc.Login)
		oidc.GET("callback", auth.backends.oidc.Callback)
	}
}

func Middleware() gin.HandlerFunc {
	if auth.sessions == nil {
		return func(c *gin.Context) {
			c.Request = attachSessionToRequest(c.Request, anonPrivileged)
		}
	}

	return func(c *gin.Context) {
		s := getSessionFromBearerToken(c.Request)
		if s == nil || s.State() != SessionStateLoggedIn {
			sendHTTPInvalidSessionResponse(c)
			c.Abort()
			return
		}
		c.Request = attachSessionToRequest(c.Request, s)
	}
}

func Healthz(ctx context.Context) (err error) {
	var messages []string
	if auth.backends.oidc != nil && auth.backends.oidc.state.get() != BackendReady {
		messages = append(messages, "oidc backend is enabled but not ready")
	}
	if auth.backends.passwd != nil && auth.backends.passwd.state.get() != BackendReady {
		messages = append(messages, "passwd backend is enabled but not ready")
	}

	if len(messages) > 0 {
		err = errors.New(strings.Join(messages, ", "))
	}
	return
}
