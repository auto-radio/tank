# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Un-nested routes for playlists, files, imports, etc. (tank#65)

### Changed

- The duration is now in seconds and is stored as a float.
- The job-timeout for the importer is now three minutes for Docker.

### Deprecated

- Nested routes for files, playlists, imports, etc. will be removed with alpha4.

## [1.0.0-alpha2] - 2023-06-20

### Added

- Add default directory `/var/audio/import` used for audio file imports via filesystem (aura#172)
- Write access, error and info logs into `TANK_LOGS` directory (tank#55)

### Changed

- Provide properties in API schemas in CamelCase notation (aura#141)
- Update all APIs to return attributes / properties in camelCase notation (aura#141)

## [1.0.0-alpha1] - 2023-02-24

Initial release.

